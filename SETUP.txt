SOURCE HOLDERS:

All webpages may have an associated source holder file located in the 'cache_dir/SUD/source_holders' folder. The source file's name is that of it's webpage's url removed of "http://" or "https://" and all "/" subsituted by a space " ".


Example:
...
  <webpage>
    <url>http://www.elehelp.com/now</url>
    <mark>1</mark>
  </webpage>
...
</webpages>
...

May possess the source file 'www.elehelp.com now.src'.
-----------------------------------------------------------------
WEBPAGE MARKING:

All webpages possess a mark tag that indicates the state of the webpage to the user. The mark may have one of the following values:

0: STATE_IDLE, the webpage is valid but isn't updated.

1: STATE_UPDATED, the webpage is valid and updated (indicated by the symbol "!").

2: STATE_PENDING, no internet connection present while the webpage is to be checked/validated. (indicated by the symbol "?").

3: STATE_INVALID, the webpage is not valid (indicated by the symbol "E").

4: STATE_UNAVAILABLE, the webpage was valid upon creation but is no longer available (indicated by the symbol "E").

Example:

<webpage>
  <url>...<url>
  <mark></mark>     //NONE: 
</webpage>

<webpage>
  <url>...<url>
  <mark>!</mark>
</webpage>

