package com.solid.wud.data;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.net.UnknownHostException;

import org.jsoup.Jsoup;

import android.graphics.Bitmap;

import com.solid.wud.utilities.BitmapTools;
import com.solid.wud.utilities.URLTools;

public class Webpage implements Serializable {
	private static final long serialVersionUID = 1L;
	private String label;
	private String url;
	private Bitmap favicon;
	private String source = null;
	private WebState state;
	private int lengthOfRawSource = 0;
	private boolean isValid;
	private boolean mustBeChecked = false;
	private boolean isBeingChecking = false;
	private int tolerance;

	public Webpage(String url, String label) {
		this(url, label, WebState.STATE_IDLE, null, 0);
	}

	public Webpage(String url, String label, WebState state, String source, int tolerance) {
		this.url = url;
		this.label = label;
		this.setFavicon(Resources.defaultFavicon);
		this.setState(state);
		this.source = source;
		this.tolerance = tolerance;
	}

	public String getURL() {
		return url;
	}

	public void setURL(String url) {
		this.url = url;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public WebState getState() {
		return state;
	}

	public int getStateIdentifier() {
		return state.ordinal();
	}

	public void setState(WebState state) {
		this.state = state;
		switch (state) {
		case STATE_INVALID:
			this.isValid = false;
			break;
		default:
			this.isValid = true;
		}
	}

	public String getSource() {
		return source;
	}

	public boolean isValid() {
		return isValid;
	}

	/**
	 * Checks if the webpage's url is valid and sets isValid accordingly. The
	 * source from the url will be saved to this webpage's source.
	 */
	public boolean validate() {
		if (!URLTools.isValid(this.url)) {
			this.setInvalid();
			return false;
		}
		this.source = getSourceFromURL();
		if (this.source != null) {
			if (this.lengthOfRawSource == 0) {
				this.setInvalid();
				return false;
			}
			this.state = WebState.STATE_IDLE;
			return true;
		}
		return false;

	}

	private void setPending() {
		this.isValid = true;
		this.mustBeChecked = true;
		this.state = WebState.STATE_PENDING;
	}

	private void setInvalid() {
		this.isValid = false;
		this.mustBeChecked = false;
		this.state = WebState.STATE_INVALID;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public boolean mustBeChecked() {
		return mustBeChecked;
	}

	public void setMustBeChecked(boolean flag) {
		this.mustBeChecked = flag;
	}

	public boolean isBeingChecked() {
		return isBeingChecking;
	}

	public void setBeingChecked(boolean isBeingChecking) {
		this.isBeingChecking = isBeingChecking;
	}

	public String getSourceFromURL() {
		StringBuilder source = new StringBuilder();
		URL t_url;
		try {
			t_url = new URL(this.url);
			if (!URLTools.isReachable(this.url))
				throw new MalformedURLException();
		} catch (MalformedURLException e) {
			this.setInvalid();
			return null;
		} catch (UnknownHostException e) {
			this.setPending();
			return null;
		}

		try {
			URLConnection urlConn = t_url.openConnection();
			InputStream is = urlConn.getInputStream();
			InputStreamReader isr = new InputStreamReader(is, "UTF-8");
			BufferedReader in = new BufferedReader(isr);
			String inputLine;
			while ((inputLine = in.readLine()) != null)
				source.append(inputLine);
			in.close();
		} catch (IOException e) {
			this.setPending();
			return null;
		}

		String sourceText = source.toString();

		this.lengthOfRawSource = sourceText.length();

		String sourceVisibleText = Jsoup.parse(sourceText).text();
		return sourceVisibleText;
	}

	public static String toProperUrlSyntax(String url) {
		String t_url = url.trim();
		if (!t_url.startsWith("http://") && !t_url.startsWith("https://"))
			t_url = "http://" + t_url;
		if (t_url.endsWith("/"))
			t_url = t_url.substring(0, t_url.length() - 1);
		return t_url;
	}

	public int getLengthOfRawSource() {
		return lengthOfRawSource;
	}

	public void setLengthOfRawSource(int length) {
		this.lengthOfRawSource = length;
	}

	public Bitmap validateFavicon() {
		Bitmap bmp = BitmapTools.getBitmapFromURL("http://" + URLTools.getHost(this.getURL()) + "/favicon.ico");
		if (bmp == null)
			bmp = Resources.defaultFavicon;
		this.setFavicon(bmp);
		return bmp;
	}

	public void setFavicon(Bitmap favicon) {
		this.favicon = favicon;
	}

	public Bitmap getFavicon() {
		return favicon;
	}

	public int gettolerance() {
		return tolerance;
	}

	public void settolerance(int tolerance) {
		this.tolerance = tolerance;
	}
}