package com.solid.wud.data;

import java.io.File;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import com.solid.wud.utilities.FileWorks;
import com.solid.wud.utilities.VersionedContentValidator;
import com.solid.wud.utilities.XMLParser;

public class DataValidator extends VersionedContentValidator<Data> {
	private File xmlDataFile;
	private String xmlData;
	private XMLParser parser;

	public DataValidator(Data data) {
		super(data);
		this.xmlDataFile = data.getXmlDataFile();
		this.xmlData = FileWorks.read(xmlDataFile);
		this.parser = new XMLParser();
	}

	@Override
	public void validateToNextVersion(int currentVersion) {
		String marker, insert;
		switch (currentVersion) {
		case 1:
			marker = "</sourceComparing>";
			insert = "<rawSourceLength>-1</rawSourceLength>";

			this.xmlData = this.xmlData.replaceAll(marker, marker + insert);
			break;
		case 2:
			//remove all <sourceComparing> tags, this operation however
			//is performed by onSave therefore omitted here.
			marker = "</sourceComparing>";
			insert = "<tolerance>0</tolerance>";

			this.xmlData = this.xmlData.replaceAll(marker, marker + insert);
			break;
		default:
			break;
		}
	}

	@Override
	protected void onValidationFinish() {
		super.getContext().setXmlData(this.xmlData);
	}

	@Override
	protected int fetchVersionFromContent() {
		int currentVersion = 1;
		if (!this.getContext().isEmpty()) {
			Document doc = parser.getDomElement(this.xmlData);
			NodeList s_nl = doc.getElementsByTagName(Data.KEY_SETUP);
			Element s_e = (Element) s_nl.item(0);
			currentVersion = Integer.parseInt(s_e.getAttribute(Data.ATTRIBUTE_VERSION));
		}
		return currentVersion;
	}

}