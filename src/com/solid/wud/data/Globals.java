package com.solid.wud.data;

import java.util.HashSet;

import android.app.Application;

import com.solid.wud.R;

public class Globals extends Application {
	private Integer sdkVersion;
	private Data data;
	private ResourceArrayValue updateValue;
	private boolean isRunning;
	private HashSet<String> updatedWebpages;
	private SourceHolder sourceHolder;
	private FaviconHolder faviconHolder;
	
	@SuppressWarnings("deprecation")
	@Override
	public void onCreate() {
		super.onCreate();
		this.sdkVersion = Integer.valueOf(android.os.Build.VERSION.SDK);
		ResourceArrayValue.updateValues = getResources().getStringArray(R.array.update_values);
		ResourceArrayValue.toleranceValues = getResources().getStringArray(R.array.tolerance_values);
		this.updatedWebpages = new HashSet<String>();
	}

	public Data getData() {
		return data;
	}

	public void setData(Data data) {
		this.data = data;
	}
	
	public void setFaviconHolder(FaviconHolder faviconHolder) {
		this.faviconHolder = faviconHolder;
	}
	
	public FaviconHolder getFaviconHolder() {
		return this.faviconHolder;
	}
	
	public void setSourceHolder(SourceHolder sourceHolder) {
		this.sourceHolder = sourceHolder;
	}
	
	public SourceHolder getSourceHolder() {
		return this.sourceHolder;
	}

	public Integer getSdkVersion() {
		return sdkVersion;
	}

	public ResourceArrayValue getUpdateValue() {
		return updateValue;
	}

	public boolean isRunning() {
		return isRunning;
	}

	public void setRunning(boolean isRunning) {
		this.isRunning = isRunning;
	}

	public HashSet<String> getUpdatedWebpages() {
		return updatedWebpages;
	}
}