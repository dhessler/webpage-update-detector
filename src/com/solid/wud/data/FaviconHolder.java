package com.solid.wud.data;

import java.io.File;
import java.io.FileOutputStream;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import com.solid.wud.utilities.AssetHolder;

public class FaviconHolder extends AssetHolder {
	private static final long serialVersionUID = 1L;

	public FaviconHolder(String folderPath) {
		super(folderPath);
	}

	@Override
	protected Object onGet(File asset) {
		return BitmapFactory.decodeFile(asset.getPath());
	}

	@Override
	protected void onSet(File asset, Object content) {
		Bitmap bmp = (Bitmap) content;
		try {
			FileOutputStream fos = new FileOutputStream(asset);
			bmp.compress(Bitmap.CompressFormat.PNG, 100, fos);
			fos.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	protected String getAssetFileName(String fromIdentifier) {
		return String.valueOf(Webpage.toProperUrlSyntax(fromIdentifier).hashCode()) + ".bmp";
	}

}
