package com.solid.wud.data;

import java.io.File;

import com.solid.wud.utilities.AssetHolder;
import com.solid.wud.utilities.FileWorks;

public class SourceHolder extends AssetHolder {
	private static final long serialVersionUID = 1L;

	public SourceHolder(String folderPath) {
		super(folderPath);
	}

	@Override
	protected Object onGet(File asset) {
		return FileWorks.read(asset);
	}

	@Override
	protected void onSet(File asset, Object content) {
		String source = (String) content;
		FileWorks.write(asset, source);
	}

	@Override
	protected String getAssetFileName(String fromIdentifier) {
		return String.valueOf(Webpage.toProperUrlSyntax(fromIdentifier).hashCode()) + ".src";
	}
}