package com.solid.wud.data;

public enum WebState {
	STATE_IDLE, //0 nothing, the webpage is valid but isn't updated
	STATE_UPDATED, //1 the webpage is valid and updated (indicated by the symbol "!")
	STATE_PENDING, //2 no internet connection present while the webpage is to be checked/validated. (indicated by the symbol "?")
	STATE_INVALID; //3 the webpage is not valid (indicated by the symbol "E")

	public static WebState getState(int identifier) {
		switch(identifier) {
		case 0 : return WebState.STATE_IDLE;
		case 1 : return WebState.STATE_UPDATED;
		case 2 : return WebState.STATE_PENDING;
		case 3 : return WebState.STATE_INVALID;
		default : return null;
		}
	}
	
	public boolean isIdle() {
		return this == STATE_IDLE;
	}
	
	public boolean isUpdated() {
		return this == STATE_UPDATED;
	}
	
	public boolean isPending() {
		return this == STATE_PENDING;
	}
	
	public boolean isInvalid() {
		return this == STATE_INVALID;
	}
}