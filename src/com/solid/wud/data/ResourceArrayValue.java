package com.solid.wud.data;

public class ResourceArrayValue {
	private static final int DEFAULT_UPDATE_SPINNER_POSITION = 6;
	private static final int DEFAULT_tolerance_SPINNER_POSITION = 7;
	public static long AVERAGE_MILLIS_IN_MONTH = 2628864000L;
	public static String[] updateValues;
	public static String[] toleranceValues;

	public static long getUpdateValue(String update) {
		int k = Integer.parseInt(update.substring(0, update.indexOf(" ")));

		int index = ResourceArrayValue.getUpdatePosition(update);

		if (index == -1)
			return -1;
		if (index <= 3)
			return k * 60000L; // minute
		if (index <= 7)
			return k * 3600000L;// hour
		return k * 86400000L;// day
	}

	public static int getUpdatePosition(String update) {
		int index;
		for (index = 0; index < updateValues.length; index++)
			if (updateValues[index].equals(update))
				break;
		if (index == updateValues.length)
			index = DEFAULT_UPDATE_SPINNER_POSITION;
		return index;
	}

	public static int gettolerancePosition(int tolerance) {
		int index;
		for (index = toleranceValues.length - 1; index > -1; index--)
			if (Integer.parseInt(toleranceValues[index]) == tolerance)
				break;
		if (index == -1)
			index = DEFAULT_tolerance_SPINNER_POSITION;
		return index;
	}
}