package com.solid.wud.data;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.Serializable;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Locale;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xmlpull.v1.XmlSerializer;

import android.graphics.Bitmap;
import android.util.Xml;

import com.solid.wud.utilities.FileWorks;
import com.solid.wud.utilities.VersionedContent;
import com.solid.wud.utilities.XMLParser;

public class Data implements VersionedContent, Serializable {
	private static final long serialVersionUID = 1L;

	// XML node keys
	protected static final String KEY_SETUP = "setup";
	protected static final String KEY_USES = "uses";
	protected static final String KEY_CATEGORY = "category";
	protected static final String KEY_TITLE = "title";
	protected static final String KEY_NOTIFICATIONS = "notifications";
	protected static final String KEY_UPDATE = "update";
	protected static final String KEY_WEBPAGES = "webpages";
	protected static final String KEY_WEBPAGE = "webpage";
	protected static final String KEY_URL = "url";
	protected static final String KEY_LABEL = "label";
	protected static final String KEY_STATE = "state";
	protected static final String KEY_SOURCE_COMPARING = "sourceComparing";
	protected static final String KEY_RAW_SOURCE_LENGTH = "rawSourceLength";
	protected static final String KEY_tolerance = "tolerance";

	// XML node ids
	protected static final String ID_BOOT_STARTUP = "boot-startup";
	protected static final String ID_MOBILE_DATA = "mobile-data";

	// XML node attributes
	protected static final String ATTRIBUTE_VERSION = "version";
	protected static final String ATTRIBUTE_LANGUAGE = "language";
	protected static final String ATTRIBUTE_USAGE_WARNINGS = "usageWarnings";

	private String xmlData;
	private ArrayList<Category> categories;
	private XMLParser parser;
	private File xmlDataFile;
	private SourceHolder sourceHolder;
	private FaviconHolder faviconHolder;
	private int version;
	private String language;
	private boolean usesMobileData;
	private boolean startupOnBoot;

	private boolean usesMobileDataUsageWarnings;

	public Data(File xmlDataFile, SourceHolder sourceHolder, FaviconHolder faviconHolder) {
		this.xmlDataFile = xmlDataFile;
		this.sourceHolder = sourceHolder;
		this.faviconHolder = faviconHolder;
		this.xmlData = FileWorks.read(xmlDataFile);
		this.parser = new XMLParser();
	}

	@Override
	public int getVersion() {
		return this.version;
	}

	@Override
	public void setVersion(int version) {
		this.version = version;
	}

	@Override
	public void load() {
		this.categories = new ArrayList<Category>();
		if (!this.isEmpty()) {
			Document doc = parser.getDomElement(this.xmlData);

			NodeList s_nl = doc.getElementsByTagName(Data.KEY_SETUP);
			Element s_e = (Element) s_nl.item(0);
			this.language = s_e.getAttribute(Data.ATTRIBUTE_LANGUAGE);

			Element mobileData_e = doc.getElementById(ID_MOBILE_DATA);
			this.usesMobileData = Boolean.parseBoolean(parser.getElementValue(mobileData_e));
			this.usesMobileDataUsageWarnings = Boolean.parseBoolean(mobileData_e
					.getAttribute(Data.ATTRIBUTE_USAGE_WARNINGS));

			Element autoStartup_e = doc.getElementById(ID_BOOT_STARTUP);
			this.startupOnBoot = Boolean.parseBoolean(parser.getElementValue(autoStartup_e));

			String title;
			boolean notifications;
			String update;
			ArrayList<Webpage> webpages;

			NodeList c_nl = doc.getElementsByTagName(Data.KEY_CATEGORY);
			Element c_e;
			NodeList w_nl;
			for (int i = 0; i < c_nl.getLength(); i++) {
				c_e = (Element) c_nl.item(i);
				title = parser.getValue(c_e, Data.KEY_TITLE);
				notifications = Boolean.parseBoolean(parser.getValue(c_e, Data.KEY_NOTIFICATIONS));
				update = parser.getValue(c_e, KEY_UPDATE);
				webpages = new ArrayList<Webpage>();

				w_nl = c_e.getElementsByTagName(Data.KEY_WEBPAGES);
				if (w_nl != null) {
					Element w_e;
					String url;
					String label;
					int state;
					Webpage webpage;
					String source;
					int rawSourceLength;
					int tolerance;
					w_nl = ((Element) w_nl.item(0)).getElementsByTagName(Data.KEY_WEBPAGE);
					for (int j = 0; j < w_nl.getLength(); j++) {
						w_e = (Element) w_nl.item(j);
						url = parser.getValue(w_e, Data.KEY_URL);
						label = parser.getValue(w_e, Data.KEY_LABEL);
						state = Integer.parseInt(parser.getValue(w_e, Data.KEY_STATE));
						rawSourceLength = Integer.parseInt(parser.getValue(w_e, Data.KEY_RAW_SOURCE_LENGTH));
						tolerance = Integer.parseInt(parser.getValue(w_e, Data.KEY_tolerance));
						webpage = new Webpage(url, label);
						Object bmp = faviconHolder.get(url);
						if (bmp != null)
							webpage.setFavicon((Bitmap) bmp);
						webpage.setState(WebState.getState(state));
						webpage.settolerance(tolerance);
						
						// for older versions
						String src_cmp = parser.getValue(w_e, Data.KEY_SOURCE_COMPARING);
						if (src_cmp.equals("false")) {
							sourceHolder.set(url, "");
							rawSourceLength = -1;
						} else {
							source = (String) sourceHolder.get(url);
							if (rawSourceLength == -1)
								try {
									source = webpage.getSourceFromURL();
								} catch (Exception e) {
									// resort to setting length of raw source to
									// simply the length of source
									webpage.setLengthOfRawSource(source.length());
								}
							else
								webpage.setLengthOfRawSource(rawSourceLength);
							webpage.setSource(source);
						}
						
						webpages.add(webpage);
					}
					Category category = new Category(title, notifications, update, webpages);
					category.validate();
					categories.add(category);
				}
			}
		} else {
			this.language = Locale.getDefault().getLanguage();
			this.usesMobileData = true;
			this.usesMobileDataUsageWarnings = true;
			this.startupOnBoot = true;
		}
	}

	@Override
	public void save() {
		XmlSerializer xmlSerializer = Xml.newSerializer();
		StringWriter writer = new StringWriter();
		try {
			xmlSerializer.setOutput(writer);
			xmlSerializer.startDocument("UTF-8", true);

			xmlSerializer.startTag("", Data.KEY_SETUP);
			xmlSerializer.attribute("", Data.ATTRIBUTE_VERSION, String.valueOf(this.version));
			xmlSerializer.attribute("", Data.ATTRIBUTE_LANGUAGE, this.language);

			xmlSerializer.startTag("", Data.KEY_USES);
			xmlSerializer.attribute("", "id", Data.ID_BOOT_STARTUP);
			xmlSerializer.text(String.valueOf(this.startupOnBoot));
			xmlSerializer.endTag("", Data.KEY_USES);

			xmlSerializer.startTag("", Data.KEY_USES);
			xmlSerializer.attribute("", "id", Data.ID_MOBILE_DATA);
			xmlSerializer
					.attribute("", Data.ATTRIBUTE_USAGE_WARNINGS, String.valueOf(this.usesMobileDataUsageWarnings));
			xmlSerializer.text(String.valueOf(this.usesMobileData));
			xmlSerializer.endTag("", Data.KEY_USES);

			for (Category category : this.categories) {
				xmlSerializer.startTag("", Data.KEY_CATEGORY);
				xmlSerializer.startTag("", Data.KEY_TITLE);
				xmlSerializer.text(category.getTitle());
				xmlSerializer.endTag("", Data.KEY_TITLE);
				xmlSerializer.startTag("", Data.KEY_NOTIFICATIONS);
				xmlSerializer.text(String.valueOf(category.notificationsEnabled()));
				xmlSerializer.endTag("", Data.KEY_NOTIFICATIONS);
				xmlSerializer.startTag("", Data.KEY_UPDATE);
				xmlSerializer.text(category.getUpdate());
				xmlSerializer.endTag("", Data.KEY_UPDATE);
				xmlSerializer.startTag("", Data.KEY_WEBPAGES);
				ArrayList<Webpage> webpages = category.getWebpages();
				for (Webpage webpage : webpages) {
					xmlSerializer.startTag("", Data.KEY_WEBPAGE);
					xmlSerializer.startTag("", Data.KEY_URL);
					xmlSerializer.text(webpage.getURL());
					xmlSerializer.endTag("", Data.KEY_URL);
					xmlSerializer.startTag("", Data.KEY_LABEL);
					xmlSerializer.text(webpage.getLabel());
					xmlSerializer.endTag("", Data.KEY_LABEL);
					xmlSerializer.startTag("", Data.KEY_STATE);
					xmlSerializer.text(String.valueOf(webpage.getState().ordinal()));
					xmlSerializer.endTag("", Data.KEY_STATE);
					xmlSerializer.startTag("", Data.KEY_RAW_SOURCE_LENGTH);
					xmlSerializer.text(String.valueOf(webpage.getLengthOfRawSource()));
					xmlSerializer.endTag("", Data.KEY_RAW_SOURCE_LENGTH);
					xmlSerializer.startTag("", Data.KEY_tolerance);
					xmlSerializer.text(String.valueOf(webpage.gettolerance()));
					xmlSerializer.endTag("", Data.KEY_tolerance);
					xmlSerializer.endTag("", Data.KEY_WEBPAGE);
				}
				xmlSerializer.endTag("", Data.KEY_WEBPAGES);
				xmlSerializer.endTag("", Data.KEY_CATEGORY);
			}
			xmlSerializer.endTag("", Data.KEY_SETUP);
			xmlSerializer.endDocument();
			FileOutputStream fos = new FileOutputStream(xmlDataFile);
			String data = writer.toString();
			this.setXmlData(data);
			fos.write(data.getBytes());
			fos.close();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public boolean isEmpty() {
		return this.xmlData == null || this.xmlData.equals("");
	}

	public File getXmlDataFile() {
		return xmlDataFile;
	}

	public String getXmlData() {
		return xmlData;
	}

	public void setXmlData(String xmlData) {
		this.xmlData = xmlData;
	}

	public ArrayList<Category> getCategories() {
		return this.categories;
	}

	public void setUsesMobileData(boolean usesMobileData) {
		this.usesMobileData = usesMobileData;
	}

	public boolean usesMobileData() {
		return this.usesMobileData;
	}

	public boolean startsUpOnBoot() {
		return this.startupOnBoot;
	}

	public void setStartupOnBoot(boolean startsUpOnBoot) {
		this.startupOnBoot = startsUpOnBoot;
	}

	public boolean usesMobileDataUsageWarnings() {
		return this.usesMobileDataUsageWarnings;
	}

	public void setUsesMobileDataUsageWarnings(boolean usesMobileDataUsageWarnings) {
		this.usesMobileDataUsageWarnings = usesMobileDataUsageWarnings;
	}

	public int getMaxMonthlyDataUsageInKB() {
		float maxMonthlyTrafficInKB = 0;
		long checksPerMonth;
		for (Category category : this.getCategories()) {
			if (category.notificationsEnabled()) {
				checksPerMonth = ResourceArrayValue.AVERAGE_MILLIS_IN_MONTH / ResourceArrayValue.getUpdateValue(category.getUpdate());
				for (Webpage webpage : category.getWebpages())
					if (webpage.isValid())
						maxMonthlyTrafficInKB += ((float) webpage.getLengthOfRawSource()) * (checksPerMonth / 512f);
			}
		}
		return (int) maxMonthlyTrafficInKB;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}
}