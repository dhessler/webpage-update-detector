package com.solid.wud.data;

import java.io.Serializable;
import java.util.ArrayList;

public class Category implements Serializable {
	private static final long serialVersionUID = 1L;
	private String title;
	private boolean notifications;
	private String update;
	private ArrayList<Webpage> webpages;
	private long lastUpdate;
	private WebState state;
	private boolean mustBeChecked = false;
	private boolean isBeingChecking = false;

	public Category(String title, boolean notifications, String update, ArrayList<Webpage> webpages) {
		this.title = title;
		this.notifications = notifications;
		this.update = update;
		this.webpages = webpages;
		this.lastUpdate = System.currentTimeMillis();
		this.setState(WebState.STATE_IDLE);
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public boolean notificationsEnabled() {
		return notifications;
	}

	public void setNotificationsEnabled(boolean flag) {
		this.notifications = flag;
	}

	public String getUpdate() {
		return update;
	}

	public void setUpdate(String update) {
		this.update = update;
	}

	public ArrayList<Webpage> getWebpages() {
		return this.webpages;
	}

	public void setWebpages(ArrayList<Webpage> webpages) {
		this.webpages = webpages;
	}

	public long getLastUpdate() {
		return this.lastUpdate;
	}

	public void setLastUpdate(long timestamp) {
		this.lastUpdate = timestamp;
	}

	public boolean mustBeChecked() {
		return mustBeChecked;
	}

	public void setMustBeChecked(boolean flag) {
		this.mustBeChecked = flag;
	}

	public boolean isBeingChecked() {
		return isBeingChecking;
	}

	public void setBeingChecked(boolean isBeingChecking) {
		this.isBeingChecking = isBeingChecking;
	}

	public boolean isUpdated() {
		boolean updated = false;
		for (Webpage w : this.getWebpages())
			if (!updated && w.getState().isUpdated())
				updated = true;
		return updated;
	}

	public WebState getState() {
		return state;
	}

	public void setState(WebState state) {
		this.state = state;
	}

	public void validate() {
		boolean updatedWebpageExists = false;
		boolean pendingWebpageExists = false;
		boolean invalidWebpageExists = false;

		for (Webpage webpage : this.getWebpages()) {
			switch (webpage.getState()) {
			case STATE_UPDATED:
				updatedWebpageExists = true;
				break;
			case STATE_PENDING:
				pendingWebpageExists = true;
				break;
			case STATE_INVALID:
				invalidWebpageExists = true;
				break;
			default:
				break;
			}
		}

		if (pendingWebpageExists)
			this.mustBeChecked = true;

		if (!updatedWebpageExists) {
			if (!pendingWebpageExists) {
				if (!invalidWebpageExists) {
					this.setState(WebState.STATE_IDLE);
				} else
					this.setState(WebState.STATE_INVALID);
			} else {
				this.setState(WebState.STATE_PENDING);
			}
		} else
			this.setState(WebState.STATE_UPDATED);
	}
}
