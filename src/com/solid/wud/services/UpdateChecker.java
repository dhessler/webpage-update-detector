package com.solid.wud.services;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.NetworkInfo.State;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;

import com.solid.wud.R;
import com.solid.wud.activities.NotificationActivity;
import com.solid.wud.communication.Action;
import com.solid.wud.communication.Broadcast;
import com.solid.wud.communication.BroadcastValues;
import com.solid.wud.communication.BroadcastingService;
import com.solid.wud.communication.Ids;
import com.solid.wud.data.Category;
import com.solid.wud.data.Data;
import com.solid.wud.data.Globals;
import com.solid.wud.data.ResourceArrayValue;
import com.solid.wud.data.WebState;
import com.solid.wud.data.Webpage;
import com.solid.wud.utilities.StringWorks;

public class UpdateChecker extends BroadcastingService {
	private static final int NOTIFIER_ID = 1;
	private static final int NOTIFICATION_ID = 2;
	private static final int UPDATE_DELAY = 300; // 5mins
	private static final int SAVE_DATA_TO_FILE_DELAY = 1800; // 30mins
	private Data data = null;
	private Globals globals;
	private boolean networkIsUp = false;
	protected boolean connectionIsWifi;
	private PendingIntent pendingIntent;
	private ScheduledExecutorService scheduledTaskExecutor;
	private ScheduledFuture<?> scheduledFuture;
	private int timeStamp; // in seconds
	private boolean isCheckingData;

	@Override
	public void onCreate() {
		super.onCreate();
		this.setupForegroundCompat();
		this.globals = (Globals) this.getApplicationContext();
		this.createAndRegisterReceivers();
		this.createNotification();
		this.timeStamp = 0;
		this.isCheckingData = false;

		this.scheduledTaskExecutor = Executors.newSingleThreadScheduledExecutor();
		this.scheduledFuture = setupScheduledTask(this.scheduledTaskExecutor);
	}

	private void createAndRegisterReceivers() {
		this.createBroadcast(Ids.ENTITY_SERVICE, new BroadcastReceiver() {
			@Override
			public synchronized void onReceive(Context context, Intent intent) {
				Action action = (Action) Broadcast.getContent(intent);
				if (action != null) {
					String id = action.getId();
					if (id.equals(BroadcastValues.KILL_SERVICE)) {
						UpdateChecker.this.stopSelf();
					} else if (id.equals(BroadcastValues.CHECK_CONNECTION)) {
						setBeingChecked(globals.getData().usesMobileData());
					} else if (id.equals(BroadcastValues.CHECK_ALL_CATEGORIES)) {
						boolean validWebpageExists;
						for (Category category : globals.getData().getCategories()) {
							validWebpageExists = false;
							if (category.getWebpages().size() > 0) {
								for (Webpage webpage : category.getWebpages())
									if (!webpage.getState().isUpdated()) {
										validWebpageExists = true;
										webpage.setMustBeChecked(true);
										if (networkIsUp)
											webpage.setBeingChecked(true);
										else
											webpage.setState(WebState.STATE_PENDING);
									}
								category.validate();
								if (validWebpageExists) {
									category.setMustBeChecked(true);
									if (networkIsUp)
										category.setBeingChecked(true);
								}
							}
						}
						sendBroadcast(Ids.ENTITY_ACTIVITY, new Action(BroadcastValues.REFRESH_ADAPTER));
						if (networkIsUp)
							postCheckData();
					} else if (id.equals(BroadcastValues.CHECK_CATEGORY)) {
						int categoryPosition = (Integer) action.getInfo()[0];
						boolean validWebpageExists = false;
						Category category = globals.getData().getCategories().get(categoryPosition);
						if (category.getWebpages().size() > 0) {
							for (Webpage webpage : category.getWebpages())
								if (!webpage.getState().isUpdated()) {
									validWebpageExists = true;
									webpage.setMustBeChecked(true);
									if (networkIsUp)
										webpage.setBeingChecked(true);
									else
										webpage.setState(WebState.STATE_PENDING);
								}
							category.validate();
							if (validWebpageExists) {
								category.setMustBeChecked(true);
								if (networkIsUp)
									category.setBeingChecked(true);
							}
							sendBroadcast(Ids.ENTITY_ACTIVITY, new Action(BroadcastValues.REFRESH_ADAPTER));
							if (networkIsUp)
								postCheckData();
						}
					} else if (id.equals(BroadcastValues.CHECK_DATA)) {
						postCheckData();
					}
				}
			}
		});

		this.createBroadcast(ConnectivityManager.CONNECTIVITY_ACTION, new BroadcastReceiver() {
			@Override
			public void onReceive(Context context, Intent intent) {
				if (globals.getData() != null) {
					Bundle extras = intent.getExtras();
					NetworkInfo info = (NetworkInfo) extras.getParcelable("networkInfo");
					State state = info.getState();
					if (state == State.CONNECTED) {
						if (info.getTypeName().equalsIgnoreCase("WIFI")) {
							connectionIsWifi = true;
							networkIsUp = setBeingChecked(true);
						} else { // mobile data
							connectionIsWifi = false;
							if (data.usesMobileData()) {
								networkIsUp = setBeingChecked(true);
							} else {
								networkIsUp = setBeingChecked(false);
							}
						}
					} else {
						networkIsUp = setBeingChecked(false);
					}
				}
			}
		});
	}

	private boolean setBeingChecked(boolean flag) {
		if (flag) {
			boolean mustBeCheckedCategoryExists = false;
			for (Category category : globals.getData().getCategories()) {
				if (category.mustBeChecked()) {
					mustBeCheckedCategoryExists = true;
					category.setBeingChecked(true);
					for (Webpage webpage : category.getWebpages())
						if (webpage.mustBeChecked())
							webpage.setBeingChecked(true);
				}
			}
			if (mustBeCheckedCategoryExists)
				postCheckData();
		} else {
			for (Category category : globals.getData().getCategories()) {
				category.setBeingChecked(false);
				for (Webpage webpage : category.getWebpages()) {
					if (webpage.isBeingChecked()) {
						webpage.setMustBeChecked(true);// TODO needed?
						webpage.setState(WebState.STATE_PENDING);
						webpage.setBeingChecked(false);
					}
				}
				category.validate();
			}
		}
		sendBroadcast(Ids.ENTITY_ACTIVITY, new Action(BroadcastValues.REFRESH_ADAPTER));
		return flag;
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		this.data = globals.getData();
		globals.setRunning(true);
		return START_STICKY;
	}

	private synchronized void checkData() {
		this.isCheckingData = true;
		boolean categoryChecked = false;
		ArrayList<Category> categories = data.getCategories();
		ArrayList<Webpage> webpages;
		Category category;
		Webpage webpage;
		int cSize = categories.size();
		int wSize;
		for (int i = 0; i < cSize; i++) {
			category = categories.get(i);
			long categoryLastUpdate = category.getLastUpdate();
			long categoryUpdateInterval = ResourceArrayValue.getUpdateValue(category.getUpdate());
			boolean categoryUpdateDelayFinished = System.currentTimeMillis() - categoryLastUpdate > categoryUpdateInterval;
			if (category.mustBeChecked() || (category.notificationsEnabled() && categoryUpdateDelayFinished)) {
				categoryChecked = true;
				webpages = category.getWebpages();
				wSize = webpages.size();
				for (int j = 0; j < wSize; j++) {
					webpage = webpages.get(j);
					if (webpage.mustBeChecked()
							|| (categoryUpdateDelayFinished && webpage.isValid() && !webpage.getState().isUpdated())) {
						String url = webpage.getURL();
						String oldSource = webpage.getSource();
						String newSource = webpage.getSourceFromURL();

						if (newSource != null && oldSource != null && !newSource.equals(oldSource)
								&& changeIsSufficient(oldSource, newSource, webpage.gettolerance())) {
							webpage.setSource(newSource);
							this.globals.getSourceHolder().set(url, newSource);
							webpage.setState(WebState.STATE_UPDATED);
							if (!category.mustBeChecked())
								this.notifyWebpageUpdate(webpage);
						} else
							webpage.setState(WebState.STATE_IDLE);
						this.setNotBeingChecked(webpage);
					}
				}
				category.validate();
				if (!category.getState().isPending()) {
					category.setMustBeChecked(false);
					category.setLastUpdate(System.currentTimeMillis());
				} else
					category.setMustBeChecked(true);
				category.setBeingChecked(false);
			}
		}

		this.isCheckingData = false;
		if (categoryChecked) {
			sendBroadcast(Ids.ENTITY_ACTIVITY, new Action(BroadcastValues.REFRESH_ADAPTER));
			this.data.save();
			// check if data usage is high while user connected through mobile
			// data
			if (this.data.usesMobileData())
				this.checkDataUsage();
		}
	}

	private boolean changeIsSufficient(String fromString, String toString, int tolerance) {
		return tolerance == 0 || StringWorks.getLevenshteinDistance(fromString, toString) >= tolerance;
	}

	private void checkDataUsage() {
		if (this.data.usesMobileDataUsageWarnings() && this.data.getMaxMonthlyDataUsageInKB() > 80 * 1024
				&& !this.connectionIsWifi) {
			this.notifyDataConsumption();
		}

	}

	private void setNotBeingChecked(Webpage webpage) {
		if (!webpage.getState().isPending())
			webpage.setMustBeChecked(false);
		else
			webpage.setMustBeChecked(true);
		if (webpage.isBeingChecked()) {
			webpage.setBeingChecked(false);
			sendBroadcast(Ids.ENTITY_ACTIVITY, new Action(BroadcastValues.REFRESH_ADAPTER));
		}
	}

	private ScheduledFuture<?> setupScheduledTask(ScheduledExecutorService scheduledTaskExecutor) {
		return scheduledTaskExecutor.scheduleAtFixedRate(new Runnable() {
			public void run() {
				if (globals.isRunning() && data != null && networkIsUp) {
					if (!isCheckingData)
						checkData();
					timeStamp += UPDATE_DELAY;
					if (timeStamp >= SAVE_DATA_TO_FILE_DELAY) {
						data.save();
						timeStamp = 0;
					}
				}
			}
		}, 2, UPDATE_DELAY, TimeUnit.SECONDS);
	}

	private void postCheckData() {
		this.scheduledFuture.cancel(false);
		this.scheduledFuture = setupScheduledTask(this.scheduledTaskExecutor);
	}

	@SuppressWarnings("deprecation")
	private void notifyDataConsumption() {
		Notification notification = new Notification(R.drawable.error_icon, getText(R.string.notification_warning),
				System.currentTimeMillis());
		notification.flags = Notification.DEFAULT_LIGHTS | Notification.FLAG_AUTO_CANCEL;
		Intent notificationIntent = new Intent(this, NotificationActivity.class);
		notificationIntent.putExtra("INFO", "NOTIFY_DATA_CONSUMPTION");
		PendingIntent pendingIntent = PendingIntent.getActivity(this, 1, notificationIntent, 0);
		notification.setLatestEventInfo(getBaseContext(), getText(R.string.notifier),
				getText(R.string.notification_warning), pendingIntent);
		this.nm.notify(NOTIFICATION_ID, notification);
	}

	@SuppressWarnings("deprecation")
	private void notifyWebpageUpdate(Webpage webpage) {
		HashSet<String> updatedWebpages = globals.getUpdatedWebpages();
		String label = webpage.getLabel();
		if (label.equals("")) {
			String urlText = webpage.getURL().replace("http://", "").replace("https://", "").replace("www.", "");
			if (urlText.length() > 15)
				urlText = urlText.substring(0, 15) + "...";
			updatedWebpages.add(urlText);
		} else {
			String labelText = webpage.getLabel();
			if (labelText.length() > 15)
				labelText = labelText.substring(0, 15) + "...";
			updatedWebpages.add(labelText);
		}

		String text = "";

		int size = updatedWebpages.size();
		Iterator<String> iterator = updatedWebpages.iterator();
		for (int i = 0; i < size - 1; i++) {
			text += iterator.next() + ", ";
		}
		text += iterator.next();

		if (size == 1)
			text += " " + getText(R.string.notification_single_update);
		else
			text += " " + getText(R.string.notification_multiple_updates);

		Notification notification = new Notification(R.drawable.updated_icon, getText(R.string.notification),
				System.currentTimeMillis());
		notification.flags = Notification.DEFAULT_LIGHTS | Notification.FLAG_AUTO_CANCEL;
		notification.setLatestEventInfo(getBaseContext(), getText(R.string.notification), text, this.pendingIntent);
		this.nm.notify(NOTIFICATION_ID, notification);
	}

	@SuppressWarnings("deprecation")
	private void createNotification() {
		nm = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
		Notification notification = new Notification(R.drawable.icon, getText(R.string.notifier),
				System.currentTimeMillis());
		Intent notificationIntent = new Intent(this, NotificationActivity.class);
		this.pendingIntent = PendingIntent.getActivity(this, 0, notificationIntent, 0);
		notification.setLatestEventInfo(getBaseContext(), getText(R.string.app_name),
				getText(R.string.notifications_enabled), this.pendingIntent);
		this.startForegroundCompat(NOTIFIER_ID, notification);
	}

	// Old onStart method that will be called on pre-5.0 platforms.
	@Override
	public void onStart(Intent intent, int startId) {
		this.onStartCommand(intent, 0, startId);
	}

	@Override
	public IBinder onBind(Intent intent) {
		return null;
	}

	@Override
	public void onDestroy() {
		this.networkIsUp = false;
		this.globals.setRunning(false);
		this.scheduledTaskExecutor.shutdown();
		this.unregisterAllReceivers();
		this.data.save();
		this.nm.cancel(R.string.notification_update);
		this.stopForegroundCompat(R.string.notification_update);
		super.onDestroy();
	}

	private static final Class<?>[] mSetForegroundSignature = new Class[] { boolean.class };
	private static final Class<?>[] mStartForegroundSignature = new Class[] { int.class, Notification.class };
	private static final Class<?>[] mStopForegroundSignature = new Class[] { boolean.class };

	private NotificationManager nm;
	private Method mSetForeground;
	private Method mStartForeground;
	private Method mStopForeground;
	private Object[] mSetForegroundArgs = new Object[1];
	private Object[] mStartForegroundArgs = new Object[2];
	private Object[] mStopForegroundArgs = new Object[1];

	private void invokeMethod(Method method, Object[] args) {
		try {
			method.invoke(this, args);
		} catch (InvocationTargetException e) {
			Log.w("ApiDemos", "Unable to invoke method", e);
		} catch (IllegalAccessException e) {
			Log.w("ApiDemos", "Unable to invoke method", e);
		}
	}

	/**
	 * This is a wrapper around the new startForeground method, using the older
	 * APIs if it is not available.
	 */
	private void startForegroundCompat(int id, Notification notification) {
		// If we have the new startForeground API, then use it.
		if (mStartForeground != null) {
			mStartForegroundArgs[0] = Integer.valueOf(id);
			mStartForegroundArgs[1] = notification;
			invokeMethod(mStartForeground, mStartForegroundArgs);
			return;
		}

		// Fall back on the old API.
		mSetForegroundArgs[0] = Boolean.TRUE;
		invokeMethod(mSetForeground, mSetForegroundArgs);
		nm.notify(id, notification);
	}

	/**
	 * This is a wrapper around the new stopForeground method, using the older
	 * APIs if it is not available.
	 */
	private void stopForegroundCompat(int id) {
		// If we have the new stopForeground API, then use it.
		if (mStopForeground != null) {
			mStopForegroundArgs[0] = Boolean.TRUE;
			invokeMethod(mStopForeground, mStopForegroundArgs);
			return;
		}

		// Fall back on the old API. Note to cancel BEFORE changing the
		// foreground state, since we could be killed at that point.
		nm.cancel(id);
		mSetForegroundArgs[0] = Boolean.FALSE;
		invokeMethod(mSetForeground, mSetForegroundArgs);
	}

	private void setupForegroundCompat() {
		try {
			mStartForeground = getClass().getMethod("startForeground", mStartForegroundSignature);
			mStopForeground = getClass().getMethod("stopForeground", mStopForegroundSignature);
			return;
		} catch (NoSuchMethodException e) {
			// Running on an older platform.
			mStartForeground = mStopForeground = null;
		}
		try {
			mSetForeground = getClass().getMethod("setForeground", mSetForegroundSignature);
		} catch (NoSuchMethodException e) {
			throw new IllegalStateException("OS doesn't have Service.startForeground OR Service.setForeground!");
		}
	}
}