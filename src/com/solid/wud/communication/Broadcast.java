package com.solid.wud.communication;

import java.io.Serializable;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;

public class Broadcast {  
	private static final String VALUE_KEY = "BROADCAST_VALUE_KEY";
	private Context context;
	private BroadcastReceiver receiver;
	private String id;
	
	public Broadcast(Context context, String id) {
		this.setContext(context);
		this.setReceiver(null);
		this.setId(id);
	}

	public Broadcast(Context context, BroadcastReceiver receiver, String id) {
		this.setContext(context);
		this.setReceiver(receiver);
		this.setId(id);
	}
	
	public void registerReceiver() {
		IntentFilter intentFilter = new IntentFilter(id);
		context.registerReceiver(receiver, intentFilter);
	}

	public void unregisterReceiver() {
		context.unregisterReceiver(receiver);
	}

	public void sendBroadcast(String toId, Serializable content) {
        Intent intent = new Intent(toId);
        intent.putExtra(VALUE_KEY, content);
        context.sendBroadcast(intent);
	}

	public static Serializable getContent(Intent intent) {
        Bundle bundle = intent.getExtras();
        return bundle.getSerializable(VALUE_KEY);
	}

	public Context getContext() {
		return context;
	}

	public void setContext(Context context) {
		this.context = context;
	}

	public BroadcastReceiver getReceiver() {
		return receiver;
	}

	public void setReceiver(BroadcastReceiver receiver) {
		this.receiver = receiver;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
}