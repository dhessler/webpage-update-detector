package com.solid.wud.communication;

import java.io.Serializable;

public interface ActionFactory {
	public Action createAction(String actionID);
	public Action createAction(String actionID, Serializable... info);
}