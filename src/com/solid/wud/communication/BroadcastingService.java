package com.solid.wud.communication;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.solid.wud.utilities.Pair;

public abstract class BroadcastingService extends Service {
	// map which associates broadcaster id to their relative broadcaster
	// accompanied by an enabled identification flag
	private Map<String, Pair<Broadcast, Boolean>> broadcasts;
	private static final String DEFAULT_BROADCAST_ID = "DEFAULT_BROADCAST_ID";
	private Broadcast defaultBroadcast;

	@Override
	public void onCreate() {
		super.onCreate();
		this.broadcasts = new HashMap<String, Pair<Broadcast, Boolean>>();
		this.defaultBroadcast = new Broadcast(this.getApplicationContext(), DEFAULT_BROADCAST_ID);
	}

	public void createBroadcast(String recieverId, BroadcastReceiver receiver) {
		Broadcast activityBroacast = new Broadcast(this.getApplicationContext(), receiver, recieverId);
		activityBroacast.registerReceiver();
		this.broadcasts.put(recieverId, new Pair<Broadcast, Boolean>(activityBroacast, true));
	}

	public void createBroadcast(String recieverId, final BroadcastReceiver receiver, boolean enabled) {
		BroadcastReceiver activityReceiver = new BroadcastReceiver() {
			@Override
			public void onReceive(Context context, Intent intent) {
				receiver.onReceive(context, intent);
			}
		};
		Broadcast activityBroacast = new Broadcast(this.getApplicationContext(), activityReceiver, recieverId);
		if (enabled)
			activityBroacast.registerReceiver();
		this.broadcasts.put(recieverId, new Pair<Broadcast, Boolean>(activityBroacast, enabled));
	}

	public void sendBroadcast(String toId, Serializable content) {
		this.defaultBroadcast.sendBroadcast(toId, content);
	}

	public void sendBroadcast(String fromId, String toId, Serializable content) {
		this.broadcasts.get(fromId).first.sendBroadcast(toId, content);
	}

	public void unregisterReceiver(String receiverId) {
		this.broadcasts.get(receiverId).first.unregisterReceiver();
		this.broadcasts.remove(receiverId);
	}
	
	public void unregisterAllReceivers() {
		Iterator<String> iterator = this.broadcasts.keySet().iterator();
		Pair<Broadcast, Boolean> broadcast;
		while(iterator.hasNext()) {
			broadcast = this.broadcasts.get(iterator.next());
			broadcast.first.unregisterReceiver();
			iterator.remove();
		}
	}

	public boolean enableReceiver(String receiverId) {
		Pair<Broadcast, Boolean> broadcast = this.broadcasts.get(receiverId);
		boolean isDisabled = !broadcast.second;
		if (isDisabled)
			this.broadcasts.get(receiverId).first.registerReceiver();
		broadcast.second = true;
		return isDisabled;
	}

	public boolean disableReceiver(String receiverId) {
		Pair<Broadcast, Boolean> broadcast = this.broadcasts.get(receiverId);
		boolean isEnabled = broadcast.second;
		if (isEnabled)
			this.broadcasts.get(receiverId).first.unregisterReceiver();
		broadcast.second = false;
		return isEnabled;
	}
	
	public void enableAllReceivers() {
		this.setAllReceiversEnabled(true);
	}
	
	public void disableAllReceivers() {
		this.setAllReceiversEnabled(false);
	}

	private void setAllReceiversEnabled(boolean flag) {
		Iterator<String> iterator = this.broadcasts.keySet().iterator();
		Pair<Broadcast, Boolean> broadcast;
		while(iterator.hasNext()) {
			broadcast = this.broadcasts.get(iterator.next());
			broadcast.second = flag;
		}
	}
}
