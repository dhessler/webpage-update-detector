package com.solid.wud.communication;

import java.io.File;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;

import com.solid.wud.R;
import com.solid.wud.data.Data;
import com.solid.wud.data.FaviconHolder;
import com.solid.wud.data.Globals;
import com.solid.wud.data.Resources;
import com.solid.wud.data.SourceHolder;

public class StartServiceOnBoot extends BroadcastReceiver {
	private Data data;
	private Context context;

	@Override
	public void onReceive(Context context, Intent intent) {
		if ("android.intent.action.BOOT_COMPLETED".equals(intent.getAction())) {
			this.context = context;
			new LoadDataTask().execute();
		}
	}

	private class LoadDataTask extends AsyncTask<Void, Void, Void> {
		private File srcDir, faviconsDir, xmlDataFile;
		private boolean isToStart;
		Globals globals;
		
		@Override
		protected void onPreExecute() {
			this.isToStart = true;
			globals = ((Globals) context.getApplicationContext());
			Resources.defaultFavicon = BitmapFactory.decodeResource(context.getResources(), R.drawable.default_favicon);
			File dir = ((ContextWrapper) context).getDir("WUD", Context.MODE_PRIVATE);
			srcDir = new File(dir, "source_holders");
			faviconsDir = new File(dir, "favicons");
			if (!srcDir.exists() || !faviconsDir.exists()) {
				this.isToStart = false;
				return;
			}
			xmlDataFile = new File(dir, "setup.xml");
		}

		@Override
		protected Void doInBackground(Void... args) {
			if (this.isToStart) {
				globals.setSourceHolder(new SourceHolder(srcDir.getPath()));
				globals.setFaviconHolder(new FaviconHolder(faviconsDir.getPath()));
				data = new Data(xmlDataFile, globals.getSourceHolder(), globals.getFaviconHolder());
				data.load();
				if (!data.startsUpOnBoot())
					this.isToStart = false;
			}
			return null;
		}

		@Override
		protected void onPostExecute(Void arg) {
			if (this.isToStart) {
				globals.setData(data);

				Intent serviceIntent = new Intent("com.solid.wud.services.UpdateChecker");
				context.startService(serviceIntent);
			}
		}
	}
}