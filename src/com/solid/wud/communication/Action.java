package com.solid.wud.communication;

import java.io.Serializable;

public class Action implements Serializable {
	private static final long serialVersionUID = 1L;
	private String id;
	private Serializable[] info;

	public Action(String id) {
		this.id = id;
		this.info = null;
	}

	public Action(String id, Serializable... info) {
		this.id = id;
		this.info = info;
	}

	public Action(String id, Serializable[] info, Serializable value) {
		this.id = id;
		this.info = info;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Serializable[] getInfo() {
		return info;
	}

	public void setInfo(Serializable[] info) {
		this.info = info;
	}

	public String toString() {
		String desc = "Action [ID=" + this.id;
		if (this.info != null) {
			desc += "; INFO=";
			for (int i = 0; i < info.length - 1; i++)
				desc += this.info[i] + ",";
			desc += this.info[info.length - 1];
		}
		return desc + "]";
	}
}