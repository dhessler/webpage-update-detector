package com.solid.wud.communication;

public final class BroadcastValues {	
	public static final String REFRESH_ADAPTER = "REFRESH_ADAPTER";
	public static final String KILL_SERVICE = "KILL_SERVICE";
	public static final String CHECK_ALL_CATEGORIES = "CHECK_ALL_CATEGORIES";
	public static final String CHECK_CATEGORY = "CHECK_CATEGORY";
	public static final String CHECK_CONNECTION = "CHECK_CONNECTION";
	public static final String CHECK_DATA = "CHECK_DATA";
}