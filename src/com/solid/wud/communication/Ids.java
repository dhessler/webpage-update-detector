package com.solid.wud.communication;

public class Ids {
	public static final String ENTITY_ACTIVITY = "ENTITY_ACTIVITY";
	public static final String ENTITY_SERVICE = "ENTITY_SERVICE";
	
	public static final String ACTIVITY_MAIN_SCREEN = "ACTIVITY_MAIN_SCREEN";
	public static final String ACTIVITY_CATEGORY_SCREEN = "ACTIVITY_CATEGORY_SCREEN";
	public static final String ACTIVITY_EDIT_CATEGORY_SCREEN = "ACTIVITY_EDIT_CATEGORY_SCREEN";
}