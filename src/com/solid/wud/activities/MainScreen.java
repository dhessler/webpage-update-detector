package com.solid.wud.activities;

import java.util.ArrayList;
import java.util.Locale;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.solid.wud.R;
import com.solid.wud.adapters.MainAdapter;
import com.solid.wud.communication.Action;
import com.solid.wud.communication.Broadcast;
import com.solid.wud.communication.BroadcastValues;
import com.solid.wud.communication.BroadcastingActivity;
import com.solid.wud.communication.Ids;
import com.solid.wud.data.Category;
import com.solid.wud.data.Data;
import com.solid.wud.data.Globals;
import com.solid.wud.data.WebState;
import com.solid.wud.data.Webpage;
import com.solid.wud.utilities.Keyboard;
import com.solid.wud.utilities.Language;
import com.solid.wud.utilities.Task;
import com.solid.wud.utilities.TextViewTools;

public class MainScreen extends BroadcastingActivity {
	private boolean currentlyVisible = false;
	private MainAdapter adapter;
	private ListView list;
	private Globals globals;
	private Data data;
	private PopupMenu popupMenu;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.page_layout);

		currentlyVisible = true;
		globals = (Globals) this.getApplicationContext();
		data = globals.getData();

		Language.setLanguage(MainScreen.this, data.getLanguage());

		// Getting adapter by passing xml data
		adapter = new MainAdapter(this, data);

		list = (ListView) findViewById(R.id.list);
		list.setLongClickable(true);
		list.setAdapter(adapter);

		list.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				Intent intent = new Intent(MainScreen.this, CategoryScreen.class);
				intent.putExtra("position", position);
				startActivity(intent);
			}
		});

		if (globals.getSdkVersion() < 11)
			registerForContextMenu(list);

		list.setOnItemLongClickListener(new OnItemLongClickListener() {
			@Override
			public boolean onItemLongClick(AdapterView<?> parent, View view, final int position, long id) {
				if (globals.getSdkVersion() >= 11)
					inflatePopupMenu(position, view);
				else {
					categoryPositionInContextMenu = position;
					list.showContextMenuForChild(parent);
					list.showContextMenu();
				}
				return true;
			}
		});

		this.createBroadcast(Ids.ENTITY_ACTIVITY, new BroadcastReceiver() {
			@Override
			public void onReceive(Context context, Intent intent) {
				Action action = (Action) Broadcast.getContent(intent);
				String id = action.getId();
				if (id.equals(BroadcastValues.REFRESH_ADAPTER)) {
					if (currentlyVisible)
						refreshAdapter();
				}
			}
		});

		if (data.getCategories().size() == 0)
			this.showGettingStartedDialog();
	}

	private void showGettingStartedDialog() {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle(R.string.getting_started);
		builder.setMessage(R.string.getting_started_message);
		builder.setCancelable(true);
		builder.setPositiveButton(R.string.OK, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {
				dialog.cancel();
			}
		});

		AlertDialog alertDialog = builder.create();
		alertDialog.show();
		TextView messageText = (TextView) alertDialog.findViewById(android.R.id.message);
		messageText.setGravity(Gravity.CENTER);

	}

	private void checkCategory(int position) {
		this.sendBroadcast(Ids.ENTITY_SERVICE, new Action(BroadcastValues.CHECK_CATEGORY, position));
	}

	private void unmarkCategory(final int position) {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setMessage(R.string.unmark_category_message);
		builder.setCancelable(true);
		builder.setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {
				dialog.cancel();
			}
		});
		builder.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {
				Category category = data.getCategories().get(position);
				ArrayList<Webpage> webpages = category.getWebpages();
				for (Webpage w : webpages)
					if (w.getState().isUpdated())
						w.setState(WebState.STATE_IDLE);
				category.validate();
				category.setLastUpdate(System.currentTimeMillis());
				data.save();
				refreshAdapter();
				dialog.cancel();
			}
		});

		AlertDialog alertDialog = builder.create();
		alertDialog.show();
		TextView messageText = (TextView) alertDialog.findViewById(android.R.id.message);
		messageText.setGravity(Gravity.CENTER);
	}

	private void editCategory(int position) {
		Intent intent = new Intent(MainScreen.this, EditCategoryScreen.class);
		intent.putExtra("position", position);
		startActivity(intent);
	}

	private void deleteCategory(final int position) {
		Builder builder = new Builder(this);
		builder.setMessage(R.string.delete_category_message);
		builder.setCancelable(true);
		builder.setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {
				dialog.cancel();
			}
		});
		builder.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {
				ArrayList<Webpage> webpages = data.getCategories().get(position).getWebpages();
				for (Webpage webpage : webpages) {
					globals.getFaviconHolder().remove(webpage.getURL());
					globals.getSourceHolder().remove(webpage.getURL());
				}
				data.getCategories().remove(position);
				data.save();
				refreshAdapter();
				dialog.cancel();
			}
		});

		AlertDialog alertDialog = builder.create();
		alertDialog.show();
		TextView messageText = (TextView) alertDialog.findViewById(android.R.id.message);
		messageText.setGravity(Gravity.CENTER);
	}

	private void refreshAdapter() {
		adapter.setData(globals.getData());
		adapter.notifyDataSetChanged();
		list.setAdapter(adapter);
	}

	@Override
	public void onPause() {
		super.onPause();
		this.disableAllReceivers();
		this.currentlyVisible = false;
	}

	@Override
	public void onResume() {
		super.onResume();
		setTitle(getText(R.string.categories));
		this.currentlyVisible = true;
		this.refreshAdapter();
		this.enableAllReceivers();
		this.handleDataConsumptionNotification();
	}

	private void handleDataConsumptionNotification() {
		Bundle bundle = this.getIntent().getExtras();
		if (bundle != null) {
			String info = bundle.getString("INFO");
			if (info != null && info.equals("NOTIFY_DATA_CONSUMPTION")) {
				this.getIntent().removeExtra("INFO");
				this.dataConsumptionAlertDialog();
			}
		}
	}

	private void dataConsumptionAlertDialog() {
		Builder builder = new Builder(this);
		builder.setMessage(R.string.data_usage_warning_message);
		builder.setCancelable(true);
		builder.setNegativeButton(R.string.close, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {
				dialog.cancel();
			}
		});
		builder.setPositiveButton(R.string.setup, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {
				dialog.cancel();
				setup();
			}
		});

		AlertDialog alertDialog = builder.create();
		alertDialog.show();
		TextView messageText = (TextView) alertDialog.findViewById(android.R.id.message);
		messageText.setGravity(Gravity.CENTER);
	}

	@Override
	public void onBackPressed() {
		this.disableAllReceivers();// TODO do they still receive in the
		super.onBackPressed();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.layout.main_menu, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.add_category:
			this.addCategory();
			return true;
		case R.id.main_check_all:
			this.checkAll();
			return true;
		case R.id.main_unmark_all:
			this.unmarkAll();
			return true;
		case R.id.delete_all:
			this.deleteAll();
			return true;
		case R.id.helpabout:
			this.helpAbout();
			return true;
		case R.id.main_setup:
			this.setup();
			return true;
		case R.id.exit:
			this.exit();
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	private void checkAll() {
		this.sendBroadcast(Ids.ENTITY_SERVICE, new Action(BroadcastValues.CHECK_ALL_CATEGORIES));
	}

	private void helpAbout() {
		Builder builder = new Builder(this);
		builder.setCancelable(true);
		builder.setNegativeButton(R.string.close, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {
				dialog.cancel();
			}
		});

		LayoutInflater inflator = LayoutInflater.from(this);
		View view = inflator.inflate(R.layout.help_about_layout, null);
		builder.setView(view);
		AlertDialog alertDialog = builder.create();
		alertDialog.show();
	}

	private void setup() {
		Builder builder = new Builder(this);
		builder.setTitle(R.string.setup);
		builder.setCancelable(true);

		LayoutInflater inflator = LayoutInflater.from(this);
		final View view = inflator.inflate(R.layout.setup_layout, null);
		final CheckBox mobileDataCheckBox = (CheckBox) view.findViewById(R.id.mobileDataCheckBox);
		final CheckBox autoStartupCheckBox = (CheckBox) view.findViewById(R.id.autoStartupCheckBox);
		final CheckBox enableWarningsCheckBox = (CheckBox) view.findViewById(R.id.enableWarningsCheckBox);
		final Spinner languageSpinner = (Spinner) view.findViewById(R.id.languageSpinner);

		final String[] indexToCode = getResources().getStringArray(R.array.language_index_to_code);
		final String currentLanguage = globals.getData().getLanguage();

		mobileDataCheckBox.setChecked(data.usesMobileData());
		autoStartupCheckBox.setChecked(data.startsUpOnBoot());
		enableWarningsCheckBox.setChecked(data.usesMobileDataUsageWarnings());

		int index;
		for (index = 0; index < indexToCode.length; index++)
			if (currentLanguage.equals(indexToCode[index]))
				break;

		languageSpinner.setSelection(index);

		builder.setNegativeButton(R.string.close, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {
				dialog.cancel();
			}
		});
		builder.setPositiveButton(R.string.apply, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {
				boolean usesMobileData = mobileDataCheckBox.isChecked();
				if (usesMobileData != data.usesMobileData()) {
					data.setUsesMobileData(mobileDataCheckBox.isChecked());
					sendBroadcast(Ids.ENTITY_SERVICE, new Action(BroadcastValues.CHECK_CONNECTION));
				}

				String newLanguage = indexToCode[languageSpinner.getSelectedItemPosition()];

				if (!newLanguage.equals(currentLanguage)) {
					globals.getData().setLanguage(newLanguage);
					Intent refresh = new Intent(MainScreen.this, MainScreen.class);
					startActivity(refresh);
					finish();
				}

				data.setStartupOnBoot(autoStartupCheckBox.isChecked());
				data.setUsesMobileDataUsageWarnings(enableWarningsCheckBox.isChecked());
				data.save();
				dialog.cancel();
			}
		});

		TextView dataUsageText = (TextView) view.findViewById(R.id.data_usage);
		int monthlyTrafficInKB = this.data.getMaxMonthlyDataUsageInKB();

		if (monthlyTrafficInKB == 0) {
			dataUsageText.append(" 0KB.");
		} else {
			dataUsageText.append("\n");
			if (monthlyTrafficInKB < 1024) {
				TextViewTools.appendColoredText(dataUsageText, String.valueOf(monthlyTrafficInKB), Color.GREEN);
				dataUsageText.append(" KB.");
			} else if (monthlyTrafficInKB < 30 * 1024) {
				TextViewTools.appendColoredText(dataUsageText, String.valueOf((int) (monthlyTrafficInKB / 1024f)),
						Color.GREEN);
				dataUsageText.append(" MB.");
			} else if (monthlyTrafficInKB < 50 * 1024) {
				TextViewTools.appendColoredText(dataUsageText, String.valueOf((int) (monthlyTrafficInKB / 1024f)),
						Color.YELLOW);
				dataUsageText.append(" MB.");
			} else {
				TextViewTools.appendColoredText(dataUsageText, String.valueOf((int) (monthlyTrafficInKB / 1024f)),
						Color.RED);
				dataUsageText.append(" MB.");
			}
		}

		builder.setView(view);
		AlertDialog alertDialog = builder.create();
		alertDialog.show();
	}

	private void exit() {
		Builder builder = new Builder(this);
		builder.setMessage(R.string.exit_message);
		builder.setCancelable(true);
		builder.setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {
				dialog.cancel();
			}
		});
		builder.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {
				sendBroadcast(Ids.ENTITY_SERVICE, new Action(BroadcastValues.KILL_SERVICE));
				finish();
			}
		});

		AlertDialog alertDialog = builder.create();
		alertDialog.show();
		TextView messageText = (TextView) alertDialog.findViewById(android.R.id.message);
		messageText.setGravity(Gravity.CENTER);
	}

	@SuppressLint("NewApi")
	private void addCategory() {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		LayoutInflater inflater = this.getLayoutInflater();
		builder.setTitle(R.string.new_category);
		final View view = inflater.inflate(R.layout.new_category_layout, null);
		final EditText titleBox = (EditText) view.findViewById(R.id.new_category_nameBox);
		final ToggleButton notificationSwitch = (ToggleButton) view.findViewById(R.id.new_category_notificationSwitch);
		final Spinner updateSpinner = (Spinner) view.findViewById(R.id.new_category_updateSpinner);

		notificationSwitch.setChecked(true);
		notificationSwitch.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if (notificationSwitch.isChecked())
					updateSpinner.setEnabled(true);
				else
					updateSpinner.setEnabled(false);
			}
		});
		updateSpinner.setSelection(6);
		builder.setView(view);
		builder.setCancelable(true);
		builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {
				Keyboard.dismiss(MainScreen.this, view);
				dialog.cancel();
			}
		});
		builder.setPositiveButton(R.string.create, new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int id) {
				Keyboard.dismiss(MainScreen.this, view);
				final String text = titleBox.getText().toString().trim().toUpperCase(Locale.getDefault());
				if (!text.equals("")) {
					new Task() {
						@Override
						protected void doInBackground() {
							String updateSpinnerText = updateSpinner.getSelectedItem().toString();
							boolean notifications = notificationSwitch.isChecked();
							saveChanges(text, notifications, updateSpinnerText);
						}

						@Override
						protected void onPostExecute() {
							data.save();
							refreshAdapter();
						}
					}.run();
				}
				dialog.cancel();
			}
		});

		AlertDialog alertDialog = builder.create();
		alertDialog.show();

		Keyboard.show(MainScreen.this, view);
	}

	private void saveChanges(String title, boolean notifications, String update) {
		Category category = new Category(title, notifications, update, new ArrayList<Webpage>());
		data.getCategories().add(category);
	}

	private void unmarkAll() {
		if (this.data.getCategories().size() > 0) {
			Builder builder = new Builder(this);
			builder.setMessage(R.string.unmark_all_categories_message);
			builder.setCancelable(true);
			builder.setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int id) {
					dialog.cancel();
				}
			});
			builder.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int id) {
					for (Category category : data.getCategories()) {
						for (Webpage webpage : category.getWebpages())
							if (webpage.getState().isUpdated())
								webpage.setState(WebState.STATE_IDLE);
						category.setLastUpdate(System.currentTimeMillis());
						category.setMustBeChecked(false);
						category.setBeingChecked(false);
						category.validate();
					}
					data.save();
					refreshAdapter();
					dialog.cancel();
				}
			});

			AlertDialog alertDialog = builder.create();
			alertDialog.show();
			TextView messageText = (TextView) alertDialog.findViewById(android.R.id.message);
			messageText.setGravity(Gravity.CENTER);
		}
	}

	private void deleteAll() {
		if (this.data.getCategories().size() > 0) {
			Builder builder = new Builder(this);
			builder.setMessage(R.string.delete_all_message);
			builder.setCancelable(true);
			builder.setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int id) {
					dialog.cancel();
				}
			});
			builder.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int id) {
					deleteAllConfirmation();
				}
			});

			AlertDialog alertDialog = builder.create();
			alertDialog.show();
			TextView messageText = (TextView) alertDialog.findViewById(android.R.id.message);
			messageText.setGravity(Gravity.CENTER);
		}
	}

	private void deleteAllConfirmation() {
		Builder builder = new Builder(this);
		builder.setMessage(R.string.delete_all_confirmation_message);
		builder.setCancelable(true);
		builder.setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {
				dialog.cancel();
			}
		});
		builder.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {
				data.getCategories().clear();
				data.save();
				refreshAdapter();
				dialog.cancel();
			}
		});

		AlertDialog alertDialog = builder.create();
		alertDialog.show();
		TextView messageText = (TextView) alertDialog.findViewById(android.R.id.message);
		messageText.setGravity(Gravity.CENTER);
	}

	private void inflatePopupMenu(final int position, View view) {
		popupMenu = new PopupMenu(this, view);
		Category category = data.getCategories().get(position);

		if (category.isUpdated())
			popupMenu.inflate(R.layout.main_popup_menu_full);
		else
			popupMenu.inflate(R.layout.main_popup_menu_simple);
		popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
			@Override
			public boolean onMenuItemClick(MenuItem item) {
				switch (item.getItemId()) {
				case R.id.main_popup_menu_check:
					checkCategory(position);
					break;
				case R.id.main_popup_menu_unmark:
					unmarkCategory(position);
					break;
				case R.id.main_popup_menu_edit:
					editCategory(position);
					break;
				case R.id.main_popup_menu_delete:
					deleteCategory(position);
					break;
				}
				return true;
			}
		});
		popupMenu.show();
	}

	// Retro compatibility for popup menu equivelent
	private int categoryPositionInContextMenu;

	@Override
	public void onCreateContextMenu(ContextMenu menu, View v, ContextMenuInfo menuInfo) {
		MenuInflater inflater = getMenuInflater();
		Category category = data.getCategories().get(categoryPositionInContextMenu);

		if (category.isUpdated())
			inflater.inflate(R.layout.main_popup_menu_full, menu);
		else
			inflater.inflate(R.layout.main_popup_menu_simple, menu);
		super.onCreateContextMenu(menu, v, menuInfo);
	}

	@Override
	public boolean onContextItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.main_popup_menu_check:
			checkCategory(categoryPositionInContextMenu);
			break;
		case R.id.main_popup_menu_unmark:
			unmarkCategory(categoryPositionInContextMenu);
			break;
		case R.id.main_popup_menu_edit:
			editCategory(categoryPositionInContextMenu);
			break;
		case R.id.main_popup_menu_delete:
			deleteCategory(categoryPositionInContextMenu);
			break;
		default:
			return super.onContextItemSelected(item);
		}
		return true;
	}
}