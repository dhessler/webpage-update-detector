package com.solid.wud.activities;

import java.io.File;
import java.io.IOException;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.ProgressBar;

import com.solid.wud.R;
import com.solid.wud.data.Data;
import com.solid.wud.data.DataValidator;
import com.solid.wud.data.FaviconHolder;
import com.solid.wud.data.Globals;
import com.solid.wud.data.Resources;
import com.solid.wud.data.SourceHolder;
import com.solid.wud.services.UpdateChecker;
import com.solid.wud.utilities.MetaData;

public class WUD extends Activity {
	private static final long LOGO_DISPLAY_TIME = 1500;
	private long timeStamp;
	private Data data;
	private File xmlDataFile;
	private File faviconsDir;
	private File srcDir;
	private Globals globals;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		globals = ((Globals) WUD.this.getApplication());
		Resources.defaultFavicon = BitmapFactory.decodeResource(getResources(), R.drawable.default_favicon);

		if (!globals.isRunning()) {
			setContentView(R.layout.splash);
			new LoadDataTask().execute();
		} else {
			Intent intent = new Intent(this, MainScreen.class);
			intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(intent);
		}
	}

	@Override
	public void onBackPressed() {
	}

	private class LoadDataTask extends AsyncTask<Void, Void, Void> {
		@Override
		protected void onPreExecute() {
			timeStamp = System.currentTimeMillis();
			File dir = getDir("WUD", Context.MODE_PRIVATE);

			/*
			 * File dir = new File(Environment.getExternalStorageDirectory(),
			 * "WUD"); if (!dir.exists()) dir.mkdir();
			 */

			faviconsDir = new File(dir, "favicons");
			if (!faviconsDir.exists())
				faviconsDir.mkdir();

			srcDir = new File(dir, "source_holders");
			if (!srcDir.exists())
				srcDir.mkdir();
			xmlDataFile = new File(dir, "setup.xml");

			if (!xmlDataFile.exists())
				try {
					xmlDataFile.createNewFile();
				} catch (IOException e) {
					e.printStackTrace();
					finish();
				}
		}

		@Override
		protected Void doInBackground(Void... args) {
			globals.setSourceHolder(new SourceHolder(srcDir.getPath()));
			globals.setFaviconHolder(new FaviconHolder(faviconsDir.getPath()));
			data = new Data(xmlDataFile, globals.getSourceHolder(), globals.getFaviconHolder());
			try {
				String value = MetaData.getValue(WUD.this, "dataVersionCode");
				int requiredDataVersion = Integer.parseInt(value);

				DataValidator dataValidator = new DataValidator(data);
				dataValidator.validateToVersion(requiredDataVersion);
			} catch (Exception e) {
				finish();
			}
			return null;
		}

		@Override
		protected void onPostExecute(Void arg) {
			data.save();
			globals.setData(data);
			Intent serviceIntent = new Intent(WUD.this, UpdateChecker.class);
			startService(serviceIntent);

			long delay = System.currentTimeMillis() - timeStamp;
			if (delay > LOGO_DISPLAY_TIME)
				delay = 0;
			else
				delay = LOGO_DISPLAY_TIME;
			final ProgressBar pBar = (ProgressBar) findViewById(R.id.loading_icon);
			if (delay != 0) {
				pBar.setVisibility(View.VISIBLE);
				pBar.getIndeterminateDrawable().setColorFilter(getResources().getColor(R.color.filtered_light_grey),
						android.graphics.PorterDuff.Mode.MULTIPLY);
			}
			Handler handler = new Handler();
			handler.postDelayed(new Runnable() {
				@Override
				public void run() {
					pBar.setIndeterminate(false);
					Intent intent = new Intent(WUD.this, MainScreen.class);
					startActivity(intent);
					WUD.this.finish();
				}
			}, delay);
		}
	}
}