package com.solid.wud.activities;

import java.util.ArrayList;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.solid.wud.R;
import com.solid.wud.adapters.CategoryAdapter;
import com.solid.wud.communication.Action;
import com.solid.wud.communication.Broadcast;
import com.solid.wud.communication.BroadcastValues;
import com.solid.wud.communication.BroadcastingActivity;
import com.solid.wud.communication.Ids;
import com.solid.wud.data.Category;
import com.solid.wud.data.Globals;
import com.solid.wud.data.ResourceArrayValue;
import com.solid.wud.data.WebState;
import com.solid.wud.data.Webpage;
import com.solid.wud.utilities.Keyboard;
import com.solid.wud.utilities.Task;

public class CategoryScreen extends BroadcastingActivity {
	private boolean currentlyVisible = false;
	private Globals globals;
	private Category category;
	private CategoryAdapter adapter;
	private ListView list;
	private int categoryPosition;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.page_layout);

		Bundle extras = getIntent().getExtras();
		categoryPosition = (Integer) extras.getInt("position");
		globals = (Globals) this.getApplicationContext();
		category = globals.getData().getCategories().get(categoryPosition);

		adapter = new CategoryAdapter(this, category);

		list = (ListView) findViewById(R.id.list);
		list.setLongClickable(true);
		list.setAdapter(adapter);

		list.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				Webpage webpage = category.getWebpages().get(position);
				String url = String.valueOf(webpage.getURL());
				Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
				startActivity(browserIntent);
				if (webpage.getState().isUpdated()) {
					unmarkWebpage(position);
				}
			}
		});

		if (globals.getSdkVersion() < 11)
			registerForContextMenu(list);

		list.setOnItemLongClickListener(new OnItemLongClickListener() {
			@Override
			public boolean onItemLongClick(AdapterView<?> parent, View view, final int position, long id) {
				if (globals.getSdkVersion() >= 11)
					inflatePopupMenu(position, view);
				else {
					webpagePositionInContextMenu = position;
					list.showContextMenuForChild(parent);
					list.showContextMenu();
				}
				return true;
			}
		});
		BroadcastReceiver activityReceiver = new BroadcastReceiver() {
			@Override
			public void onReceive(Context context, Intent intent) {
				Action action = (Action) Broadcast.getContent(intent);
				if (action.getId().equals(BroadcastValues.REFRESH_ADAPTER)) {
					if (currentlyVisible)
						refreshAdapter();
				}
			}
		};
		currentlyVisible = true;
		this.createBroadcast(Ids.ENTITY_ACTIVITY, activityReceiver);
	}

	private void checkWebpage(int position) {
		Webpage webpage = this.category.getWebpages().get(position);
		webpage.setState(WebState.STATE_IDLE);
		webpage.setBeingChecked(true);
		webpage.setMustBeChecked(true);
		this.category.setBeingChecked(true);
		this.category.setMustBeChecked(true);
		refreshAdapter();
		sendBroadcast(Ids.ENTITY_ACTIVITY, new Action(BroadcastValues.CHECK_DATA));
	}

	private void unmarkWebpage(final int position) {
		final Webpage webpage = category.getWebpages().get(position);
		new Task() {
			@Override
			protected void doInBackground() {
				webpage.setState(WebState.STATE_IDLE);
				sendBroadcast(Ids.ENTITY_ACTIVITY, new Action(BroadcastValues.REFRESH_ADAPTER));
				webpage.validate();
			}

			@Override
			protected void onPostExecute() {
				category.validate();
				globals.getData().save();
				sendBroadcast(Ids.ENTITY_ACTIVITY, new Action(BroadcastValues.REFRESH_ADAPTER));
			}
		}.run();
	}

	private void editWebpage(final int position) {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		LayoutInflater inflater = this.getLayoutInflater();
		builder.setTitle(R.string.edit_webpage);
		final View view = inflater.inflate(R.layout.new_webpage_layout, null);
		final EditText labelBox = (EditText) view.findViewById(R.id.new_webpage_labelBox);
		final EditText urlBox = (EditText) view.findViewById(R.id.new_webpage_urlBox);
		final Webpage webpage = category.getWebpages().get(position);
		final ToggleButton toleranceEnabled = (ToggleButton) view.findViewById(R.id.new_webpage_toleranceSwitch);
		final Spinner toleranceSpinner = (Spinner) view.findViewById(R.id.new_webpage_toleranceSpinner);

		int tolerance = webpage.gettolerance();

		if (tolerance == 0) {
			toleranceEnabled.setChecked(false);
			toleranceSpinner.setEnabled(false);
			toleranceSpinner.setSelection(ResourceArrayValue.gettolerancePosition(0));
		} else {
			toleranceEnabled.setChecked(true);
			toleranceSpinner.setEnabled(true);
			toleranceSpinner.setSelection(ResourceArrayValue.gettolerancePosition(tolerance));
		}

		toleranceEnabled.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				toleranceSpinner.setEnabled(toleranceEnabled.isChecked());
				if (!toleranceSpinner.isEnabled())
					toleranceSpinner.setSelection(ResourceArrayValue.gettolerancePosition(0));
			}
		});

		labelBox.setText(webpage.getLabel());
		urlBox.setText(webpage.getURL().replace("http://", "").replace("https://", ""));
		builder.setView(view);
		builder.setCancelable(true);
		builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {
				Keyboard.dismiss(CategoryScreen.this, view);
				dialog.cancel();
			}
		});
		builder.setPositiveButton(R.string.save, new DialogInterface.OnClickListener() {
			@Override
			public void onClick(final DialogInterface dialog, int id) {
				Keyboard.dismiss(CategoryScreen.this, view);
				final String labelText = labelBox.getText().toString();
				final String urlText = urlBox.getText().toString();
				if (toleranceEnabled.isChecked())
					webpage.settolerance(Integer.parseInt(toleranceSpinner.getSelectedItem().toString()));
				else
					webpage.settolerance(0);
				if (!labelText.equals(webpage.getLabel())
						|| !Webpage.toProperUrlSyntax(urlText).equals(webpage.getURL()))
					new Task() {
						@Override
						protected void doInBackground() {
							webpage.setBeingChecked(true);
							category.setBeingChecked(true);
							sendBroadcast(Ids.ENTITY_ACTIVITY, new Action(BroadcastValues.REFRESH_ADAPTER));
							saveChanges(webpage, labelText, urlText);
						}

						@Override
						protected void onPostExecute() {
							webpage.setBeingChecked(false);
							category.setBeingChecked(false);
							sendBroadcast(Ids.ENTITY_ACTIVITY, new Action(BroadcastValues.REFRESH_ADAPTER));
							globals.getData().save();
						}
					}.run();
				dialog.cancel();
			}

			private void saveChanges(Webpage webpage, String labelText, String urlText) {
				webpage.setLabel(labelText);
				globals.getFaviconHolder().remove(webpage.getURL());
				globals.getSourceHolder().remove(webpage.getURL());
				webpage.setURL(Webpage.toProperUrlSyntax(urlText));
				Bitmap favicon = webpage.validateFavicon();
				if (favicon != null)
					globals.getFaviconHolder().set(webpage.getURL(), favicon);
				sendBroadcast(Ids.ENTITY_ACTIVITY, new Action(BroadcastValues.REFRESH_ADAPTER));
				webpage.validate();
				if (webpage.isValid() && webpage.getSource() != null)
					globals.getSourceHolder().set(webpage.getURL(), webpage.getSource());
				category.validate();

			}
		});

		AlertDialog alertDialog = builder.create();
		alertDialog.show();

		Keyboard.show(this, view);
	}

	private void deleteWebpage(final int position) {
		Webpage webpage = this.category.getWebpages().get(position);
		globals.getFaviconHolder().remove(webpage.getURL());
		globals.getSourceHolder().remove(webpage.getURL());
		this.category.getWebpages().remove(position);
		this.globals.getData().save();
		this.refreshAdapter();
	}

	private void refreshAdapter() {
		this.adapter.setCategory(globals.getData().getCategories().get(categoryPosition));
		this.adapter.notifyDataSetChanged();
		this.list.setAdapter(adapter);
	}

	@Override
	public void onPause() {
		super.onPause();
		this.currentlyVisible = false;
	}

	@Override
	public void onResume() {
		super.onResume();
		this.setTitle(this.globals.getData().getCategories().get(this.categoryPosition).getTitle());
		this.refreshAdapter();
		this.currentlyVisible = true;
	}

	private void editCategory() {
		Intent intent = new Intent(CategoryScreen.this, EditCategoryScreen.class);
		intent.putExtra("position", categoryPosition);
		startActivity(intent);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.layout.category_menu, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.add_webpage:
			addWebpage();
			return true;
		case R.id.category_check_all:
			checkAll();
			return true;
		case R.id.category_unmark_all:
			unmarkAll();
			return true;
		case R.id.category_edit:
			editCategory();
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	private void checkAll() {
		if (this.category.getWebpages().size() > 0)
			this.sendBroadcast(Ids.ENTITY_SERVICE, new Action(BroadcastValues.CHECK_CATEGORY, this.categoryPosition));
	}

	private void addWebpage() {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		LayoutInflater inflater = this.getLayoutInflater();
		builder.setTitle(R.string.new_webpage);
		final View view = inflater.inflate(R.layout.new_webpage_layout, null);
		builder.setView(view);
		builder.setCancelable(true);

		final EditText labelBox = (EditText) view.findViewById(R.id.new_webpage_labelBox);
		final EditText urlBox = (EditText) view.findViewById(R.id.new_webpage_urlBox);
		final ToggleButton toleranceEnabled = (ToggleButton) view.findViewById(R.id.new_webpage_toleranceSwitch);
		final Spinner toleranceSpinner = (Spinner) view.findViewById(R.id.new_webpage_toleranceSpinner);
		toleranceSpinner.setEnabled(false);
		toleranceSpinner.setSelection(ResourceArrayValue.gettolerancePosition(0));

		toleranceEnabled.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				toleranceSpinner.setEnabled(toleranceEnabled.isChecked());
				if (!toleranceSpinner.isEnabled())
					toleranceSpinner.setSelection(ResourceArrayValue.gettolerancePosition(0));
			}
		});

		builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {
				Keyboard.dismiss(CategoryScreen.this, view);
				dialog.cancel();
			}
		});

		builder.setPositiveButton(R.string.add, new DialogInterface.OnClickListener() {
			@Override
			public void onClick(final DialogInterface dialog, int id) {
				Keyboard.dismiss(CategoryScreen.this, view);
				final String urlText = urlBox.getText().toString();
				final String label = labelBox.getText().toString();
				if (urlText != null && !urlText.equals("")) {
					final String url = Webpage.toProperUrlSyntax(urlText);
					final Webpage webpage = new Webpage(url, label);
					webpage.settolerance(Integer.parseInt(toleranceSpinner.getSelectedItem().toString()));
					new Task() {
						@Override
						protected void doInBackground() {
							category.getWebpages().add(webpage);
							webpage.setBeingChecked(true);
							category.setBeingChecked(true);
							sendBroadcast(Ids.ENTITY_ACTIVITY, new Action(BroadcastValues.REFRESH_ADAPTER));
							Bitmap favicon = webpage.validateFavicon();
							if (favicon != null)
								globals.getFaviconHolder().set(webpage.getURL(), favicon);
							sendBroadcast(Ids.ENTITY_ACTIVITY, new Action(BroadcastValues.REFRESH_ADAPTER));
							webpage.validate();
							if (webpage.isValid() && webpage.getSource() != null)
								globals.getSourceHolder().set(url, webpage.getSource());
							category.validate();
						}

						@Override
						protected void onPostExecute() {
							webpage.setBeingChecked(false);
							category.setBeingChecked(false);
							sendBroadcast(Ids.ENTITY_ACTIVITY, new Action(BroadcastValues.REFRESH_ADAPTER));
							globals.getData().save();
						}
					}.run();
					dialog.cancel();
				}
			}
		});

		AlertDialog alertDialog = builder.create();
		alertDialog.show();

		Keyboard.show(this, view);
	}

	private void unmarkAll() {
		if (category.getWebpages().size() > 0) {
			Builder builder = new Builder(CategoryScreen.this);
			builder.setMessage(R.string.unmark_all_webpages_message);
			builder.setCancelable(true);
			builder.setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int id) {
					dialog.cancel();
				}
			});
			builder.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int id) {
					new Task() {
						private ArrayList<Integer> webpageIndexesToValidate = new ArrayList<Integer>();
						private ArrayList<Webpage> webpages = category.getWebpages();

						@Override
						protected void onPreExecute() {
							for (int i = 0; i < category.getWebpages().size(); i++) {
								Webpage webpage = webpages.get(i);
								if (webpage.getState().isUpdated()) {
									webpage.setState(WebState.STATE_IDLE);
									webpageIndexesToValidate.add(i);
								}
							}
							sendBroadcast(Ids.ENTITY_ACTIVITY, new Action(BroadcastValues.REFRESH_ADAPTER));
						}

						@Override
						protected void doInBackground() {
							for (int i : webpageIndexesToValidate) {
								Webpage webpage = webpages.get(i);
								webpage.validate();
							}
						}

						@Override
						protected void onPostExecute() {
							category.validate();
							globals.getData().save();
							sendBroadcast(Ids.ENTITY_ACTIVITY, new Action(BroadcastValues.REFRESH_ADAPTER));
						}
					}.run();
					dialog.cancel();
				}
			});

			AlertDialog alertDialog = builder.create();
			alertDialog.show();
			TextView messageText = (TextView) alertDialog.findViewById(android.R.id.message);
			messageText.setGravity(Gravity.CENTER);
		}
	}

	private void inflatePopupMenu(final int position, View view) {
		PopupMenu popupMenu = new PopupMenu(CategoryScreen.this, view);
		Webpage webpage = category.getWebpages().get(position);
		if (webpage.getState().isUpdated())
			popupMenu.inflate(R.layout.category_popup_menu_full);
		else
			popupMenu.inflate(R.layout.category_popup_menu_simple);

		popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
			@Override
			public boolean onMenuItemClick(MenuItem item) {
				switch (item.getItemId()) {
				case R.id.category_popup_menu_check:
					checkWebpage(position);
					break;
				case R.id.category_popup_menu_unmark:
					unmarkWebpage(position);
					break;
				case R.id.category_popup_menu_edit:
					editWebpage(position);
					break;
				case R.id.category_popup_menu_delete:
					deleteWebpage(position);
					break;
				}
				return true;
			}
		});
		popupMenu.show();
	}

	// Retro compatibility for popup menu equivelent
	private int webpagePositionInContextMenu;

	@Override
	public void onCreateContextMenu(ContextMenu menu, View v, ContextMenuInfo menuInfo) {
		MenuInflater inflater = getMenuInflater();
		Webpage webpage = this.category.getWebpages().get(webpagePositionInContextMenu);
		if (webpage.getState().isUpdated())
			inflater.inflate(R.layout.category_popup_menu_full, menu);
		else
			inflater.inflate(R.layout.category_popup_menu_simple, menu);
		super.onCreateContextMenu(menu, v, menuInfo);
	}

	@Override
	public boolean onContextItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.category_popup_menu_unmark:
			unmarkWebpage(webpagePositionInContextMenu);
			break;
		case R.id.category_popup_menu_edit:
			editWebpage(webpagePositionInContextMenu);
			break;
		case R.id.category_popup_menu_delete:
			deleteWebpage(webpagePositionInContextMenu);
			break;
		default:
			return super.onContextItemSelected(item);
		}
		return true;
	}
}