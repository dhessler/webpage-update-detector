package com.solid.wud.activities;

import java.util.ArrayList;
import java.util.Locale;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.SparseArray;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.ToggleButton;

import com.solid.wud.R;
import com.solid.wud.adapters.EditCategoryAdapter;
import com.solid.wud.communication.Action;
import com.solid.wud.communication.BroadcastValues;
import com.solid.wud.communication.BroadcastingActivity;
import com.solid.wud.communication.Ids;
import com.solid.wud.data.Category;
import com.solid.wud.data.Globals;
import com.solid.wud.data.ResourceArrayValue;
import com.solid.wud.data.Webpage;
import com.solid.wud.utilities.Task;

public class EditCategoryScreen extends BroadcastingActivity {
	private int categoryPosition;
	private Globals globals;
	private EditCategoryAdapter adapter;
	private ListView list;
	boolean valueChanged = false;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.edit_category_layout);

		Bundle extras = getIntent().getExtras();
		categoryPosition = (Integer) extras.getInt("position");
		globals = (Globals) this.getApplicationContext();

		this.setTitle(R.string.edit_category);

		final EditText titleBox = (EditText) findViewById(R.id.edit_category_nameBox);
		final ToggleButton notificationSwitch = (ToggleButton) findViewById(R.id.edit_category_notificationSwitch);
		final Spinner updateSpinner = (Spinner) findViewById(R.id.edit_category_updateSpinner);

		final Category category = globals.getData().getCategories().get(categoryPosition);

		titleBox.setText(category.getTitle());

		list = (ListView) findViewById(R.id.edit_category_list);
		list.setItemsCanFocus(true);
		list.setClickable(false);
		adapter = new EditCategoryAdapter(this, category);
		list.setAdapter(adapter);

		if (category.notificationsEnabled()) {
			notificationSwitch.setChecked(true);
			updateSpinner.setEnabled(true);
		} else {
			notificationSwitch.setChecked(false);
			updateSpinner.setEnabled(false);
		}
		notificationSwitch.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if (notificationSwitch.isChecked())
					updateSpinner.setEnabled(true);
				else
					updateSpinner.setEnabled(false);
			}
		});

		updateSpinner.setSelection(ResourceArrayValue.getUpdatePosition(category.getUpdate()));

		Button cancelButton = (Button) this.findViewById(R.id.cancelButton);
		cancelButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				finish();
			}
		});

		final Button saveButton = (Button) this.findViewById(R.id.saveButton);
		saveButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				v.getRootView().requestFocus();
				String title = titleBox.getText().toString().trim().toUpperCase(Locale.getDefault());
				boolean notifications = notificationSwitch.isChecked();
				String update = updateSpinner.getSelectedItem().toString();

				if (!category.getTitle().equals(title)) {
					category.setTitle(title);
					valueChanged = true;
				}
				if (category.notificationsEnabled() != notifications) {
					category.setNotificationsEnabled(notifications);
					valueChanged = true;
				}
				if (!category.getUpdate().equals(update)) {
					category.setUpdate(update);
					valueChanged = true;
				}

				final SparseArray<String> position2labelValue = adapter.getPosition2labelValue();
				final SparseArray<String> position2urlValue = adapter.getPosition2urlValue();

				new Task() {
					@Override
					protected void doInBackground() {
						saveChanges(valueChanged, position2labelValue, position2urlValue);
					}

					@Override
					protected void onPostExecute() {
						sendBroadcast(Ids.ENTITY_ACTIVITY, new Action(BroadcastValues.REFRESH_ADAPTER));
					}
				}.run();
				finish();
			}

			private void saveChanges(boolean changeHasOccured, SparseArray<String> position2labelValue,
					SparseArray<String> position2urlValue) {
				ArrayList<Webpage> webpages = category.getWebpages();
				Webpage webpage;
				int position;

				int modifiedLabelsArraySize = position2labelValue.size();
				String label;
				for (int i = 0; i < modifiedLabelsArraySize; i++) {
					position = position2labelValue.keyAt(i);
					label = position2labelValue.get(position);
					webpages.get(position).setLabel(label);
				}

				int modifiedUrlsArraySize = position2urlValue.size();

				if (modifiedUrlsArraySize > 0) {
					String url;

					for (int i = 0; i < modifiedUrlsArraySize; i++) {
						position = position2urlValue.keyAt(i);
						url = position2urlValue.get(position);
						webpage = webpages.get(position);
						webpage.setBeingChecked(true);
					}
					category.setBeingChecked(true);

					sendBroadcast(Ids.ENTITY_ACTIVITY, new Action(BroadcastValues.REFRESH_ADAPTER));

					for (int i = 0; i < modifiedUrlsArraySize; i++) {
						position = position2urlValue.keyAt(i);
						url = position2urlValue.get(position);
						webpage = webpages.get(position);
						globals.getFaviconHolder().remove(webpage.getURL());
						globals.getSourceHolder().remove(webpage.getURL());
						webpage.setURL(Webpage.toProperUrlSyntax(url));
						Bitmap favicon = webpage.validateFavicon();
						if (favicon != null)
							globals.getFaviconHolder().set(webpage.getURL(), favicon);
						sendBroadcast(Ids.ENTITY_ACTIVITY, new Action(BroadcastValues.REFRESH_ADAPTER));
						webpage.validate();
						if (webpage.isValid() && webpage.getSource() != null)
							globals.getSourceHolder().set(url, webpage.getSource());
						webpage.setBeingChecked(false);
						sendBroadcast(Ids.ENTITY_ACTIVITY, new Action(BroadcastValues.REFRESH_ADAPTER));
					}

					category.validate();
					category.setBeingChecked(false);
					sendBroadcast(Ids.ENTITY_ACTIVITY, new Action(BroadcastValues.REFRESH_ADAPTER));
				}
				if (changeHasOccured || modifiedLabelsArraySize > 0 || modifiedUrlsArraySize > 0)
					globals.getData().save();
			}
		});
		titleBox.requestFocus();
	}
}
