package com.solid.wud.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import com.solid.wud.data.Globals;

public class NotificationActivity extends Activity {
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Globals globals = (Globals) this.getApplicationContext();
		globals.getUpdatedWebpages().clear();
		Intent intent = new Intent(this.getApplicationContext(), MainScreen.class);
		Bundle bundle = this.getIntent().getExtras();
		if (bundle != null) {
			String info = bundle.getString("INFO");
			if (info != null)
				intent.putExtra("INFO", bundle.getString("INFO"));
		}
		if (isTaskRoot())
			intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		else 
			intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		startActivity(intent);
		finish();
	}
}