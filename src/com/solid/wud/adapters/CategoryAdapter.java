package com.solid.wud.adapters;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.solid.wud.R;
import com.solid.wud.data.Category;
import com.solid.wud.data.Webpage;
import com.solid.wud.utilities.TextViewTools;

public class CategoryAdapter extends BaseAdapter {
	private Activity activity;
	private Category category;
	private LayoutInflater inflater = null;

	public CategoryAdapter(Activity activity, Category category) {
		this.activity = activity;
		this.category = category;
		this.inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	public void setCategory(Category category) {
		this.category = category;
	}

	public View getView(int position, View convertView, ViewGroup parent) {
		View vi = convertView;
		if (convertView == null)
			vi = inflater.inflate(R.layout.category_list_item, parent, false);
		ImageView favicon = (ImageView) vi.findViewById(R.id.webpage_favicon);
		TextView label = (TextView) vi.findViewById(R.id.webpage_label);
		ImageView icon = (ImageView) vi.findViewById(R.id.webpage_icon);
		ProgressBar pBar = (ProgressBar) vi.findViewById(R.id.loading_icon);

		Webpage webpage = category.getWebpages().get(position);

		String labelText = webpage.getLabel();
		if (labelText.equals("")) {
			String text = String.valueOf(webpage.getURL()).replace("http://", "").replace("https://", "");
			TextViewTools.setTextWithLineLimit(label, text, 20);
		} else
			label.setText(webpage.getLabel());

		// first item icon fix
		favicon.setImageBitmap(null);
		icon.setImageBitmap(null);

		favicon.setImageBitmap(webpage.getFavicon());

		if (webpage.isBeingChecked()) {
			pBar.setVisibility(View.VISIBLE);
		} else {
			pBar.setVisibility(View.GONE);

			Bitmap sIcon = null;
			switch (webpage.getState()) {
			case STATE_UPDATED:
				sIcon = BitmapFactory.decodeResource(activity.getResources(), R.drawable.updated_icon);
				break;
			case STATE_PENDING:
				sIcon = BitmapFactory.decodeResource(activity.getResources(), R.drawable.pending_icon);
				break;
			case STATE_INVALID:
				sIcon = BitmapFactory.decodeResource(activity.getResources(), R.drawable.error_icon);
				break;
			default:
				break;
			}
			icon.setImageBitmap(sIcon);
		}

		return vi;
	}

	public int getCount() {
		return category.getWebpages().size();
	}

	public Object getItem(int position) {
		return position;
	}

	public long getItemId(int position) {
		return position;
	}

	public LayoutInflater getInflater() {
		return inflater;
	}
}