package com.solid.wud.adapters;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.solid.wud.R;
import com.solid.wud.data.Category;
import com.solid.wud.data.Data;
import com.solid.wud.data.Webpage;
import com.solid.wud.utilities.TextViewTools;

public class MainAdapter extends BaseAdapter {
	private Activity activity;
	private LayoutInflater inflater = null;
	private Data data;

	public MainAdapter(Activity activity, Data data) {
		this.activity = activity;
		this.data = data;
		this.inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	public void setData(Data data) {
		this.data = data;
	}

	public View getView(int position, View convertView, ViewGroup parent) {
		View vi = convertView;
		if (convertView == null)
			vi = inflater.inflate(R.layout.main_list_item, parent, false);

		TextView title = (TextView) vi.findViewById(R.id.main_category_title);
		TextView labels = (TextView) vi.findViewById(R.id.labels);
		ImageView icon1 = (ImageView) vi.findViewById(R.id.main_category_icon1);
		ImageView icon2 = (ImageView) vi.findViewById(R.id.main_category_icon2);
		ProgressBar pBar = (ProgressBar) vi.findViewById(R.id.loading_icon);

		Category category = data.getCategories().get(position);

		String labels_text = "";
		boolean updated = false;
		TextViewTools.setTextWithLineLimit(title, category.getTitle(), 1);
		ArrayList<Webpage> ws = category.getWebpages();
		for (Webpage w : ws) {
			if (w.getLabel().equals(""))
				labels_text += w.getURL().replace("http://", "").replace("https://", "") + "  �  ";
			else
				labels_text += w.getLabel() + "  �  ";
			if (!updated && w.getState().isUpdated())
				updated = true;
		}
		if (labels_text.indexOf("�") != -1)
			labels_text = labels_text.substring(0, labels_text.length() - 5);

		TextViewTools.setTextWithLineLimit(labels, labels_text, 5);

		// first item icon fix
		icon1.setImageBitmap(null);
		icon2.setImageBitmap(null);

		if (category.notificationsEnabled()) {
			Bitmap nIcon = BitmapFactory.decodeResource(activity.getResources(), R.drawable.notification_icon);
			icon2.setImageBitmap(nIcon);
		}

		if (category.isBeingChecked()) {
			pBar.setVisibility(View.VISIBLE);
			if (category.notificationsEnabled()) {
				float r_px = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 22, activity.getResources()
						.getDisplayMetrics());
				float t_px = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 4, activity.getResources()
								.getDisplayMetrics());
				RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) pBar.getLayoutParams();
				params.setMargins(0, (int) t_px, (int) r_px, 0);
				pBar.setLayoutParams(params);
			}
		} else {
			pBar.setVisibility(View.GONE);
			Bitmap sIcon = null;
			switch (category.getState()) {
			case STATE_UPDATED:
				sIcon = BitmapFactory.decodeResource(activity.getResources(), R.drawable.updated_icon);
				break;
			case STATE_PENDING:
				sIcon = BitmapFactory.decodeResource(activity.getResources(), R.drawable.pending_icon);
				break;
			case STATE_INVALID:
				sIcon = BitmapFactory.decodeResource(activity.getResources(), R.drawable.error_icon);
				break;
			default:
				break;
			}
			if (!category.notificationsEnabled())
				icon2.setImageBitmap(sIcon);
			else
				icon1.setImageBitmap(sIcon);
		}
		return vi;
	}

	public int getCount() {
		return data.getCategories().size();
	}

	public Object getItem(int position) {
		return position;
	}

	public long getItemId(int position) {
		return position;
	}

	public LayoutInflater getInflater() {
		return inflater;
	}
}