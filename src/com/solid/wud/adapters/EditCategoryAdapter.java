package com.solid.wud.adapters;

import android.app.Activity;
import android.content.Context;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnFocusChangeListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.EditText;

import com.solid.wud.R;
import com.solid.wud.data.Category;
import com.solid.wud.data.Webpage;

public class EditCategoryAdapter extends BaseAdapter {
	private LayoutInflater inflater;
	private Category category;
	private SparseArray<String> position2labelValue;
	private SparseArray<String> position2urlValue;
	
	public EditCategoryAdapter(Activity activity, Category category) {
		this.category = category;
		this.position2labelValue = new SparseArray<String>();
		this.position2urlValue = new SparseArray<String>();
		this.inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	public int getCount() {
		return this.category.getWebpages().size();
	}

	public Object getItem(int position) {
		return position;
	}

	public long getItemId(int position) {
		return position;
	}

	public View getView(final int position, View convertView, ViewGroup parent) {
		if (convertView == null)
			convertView = inflater.inflate(R.layout.edit_category_list_item, parent, false);
		final EditText labelBox = (EditText) convertView.findViewById(R.id.edit_category_webpageLabelBox);
		final EditText urlBox = (EditText) convertView.findViewById(R.id.edit_category_webpageURLBox);

		final Webpage webpage = this.category.getWebpages().get(position);
		labelBox.setText(webpage.getLabel());
		urlBox.setText(webpage.getURL().replace("http://", "").replace("https://", ""));

		labelBox.setOnFocusChangeListener(new OnFocusChangeListener() {
			public void onFocusChange(View v, boolean hasFocus) {
				if (!hasFocus) {
					String text = labelBox.getText().toString();
					if (!webpage.getLabel().equals(text))
						position2labelValue.put(position, text);
				}
			}
		});

		urlBox.setOnFocusChangeListener(new OnFocusChangeListener() {
			public void onFocusChange(View v, boolean hasFocus) {
				if (!hasFocus) {
					String text = urlBox.getText().toString();
					if (!webpage.getURL().equals(text))
						position2urlValue.put(position, text);
				}
			}
		});

		return convertView;
	}

	public SparseArray<String> getPosition2labelValue() {
		return position2labelValue;
	}

	public SparseArray<String> getPosition2urlValue() {
		return position2urlValue;
	}
}