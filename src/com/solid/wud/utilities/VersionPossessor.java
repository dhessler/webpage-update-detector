package com.solid.wud.utilities;

public interface VersionPossessor {
	public int getVersion();
	public void setVersion(int version);
}
