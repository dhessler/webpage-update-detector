package com.solid.wud.utilities;

import android.os.AsyncTask;

public abstract class Task extends AsyncTask<Void, Void, Void> {
	@Override
	protected Void doInBackground(Void... params) {
		this.doInBackground();
		return null;
	}

	@Override
	protected void onPostExecute(Void arg) {
		this.onPostExecute();
	}

	protected abstract void doInBackground();

	protected abstract void onPostExecute();

	public void run() {
		this.execute();
	}

}
