package com.solid.wud.utilities;

import java.util.Locale;

import android.content.Context;
import android.content.res.Configuration;

public class Language {
	public static void setLanguage(Context context, String countryCode) {
		Locale locale = new Locale(countryCode);
		Locale.setDefault(locale);
		Configuration config = new Configuration();
		config.locale = locale;
		context.getApplicationContext().getResources().updateConfiguration(config, null);
	}
}
