package com.solid.wud.utilities;

public abstract class VersionedContentValidator<T extends VersionedContent> {
	private T context;

	public VersionedContentValidator(T context) {
		this.context = context;
	}

	public void validateToVersion(int toVersion) {
		if (this.getContext().isEmpty()) {
			this.getContext().load();
			this.getContext().setVersion(toVersion);
			return;
		} else {
			int currentVersion = this.fetchVersionFromContent();
			if (toVersion < currentVersion)
				return;
			if (toVersion > currentVersion) {
				while (currentVersion < toVersion) {
					this.validateToNextVersion(currentVersion);
					currentVersion++;
				}
				this.onValidationFinish();
			}
			this.getContext().load();
			this.getContext().setVersion(toVersion);
		}
	}

	protected T getContext() {
		return context;
	}

	protected abstract void validateToNextVersion(int currentVersion);

	protected abstract void onValidationFinish();

	protected abstract int fetchVersionFromContent();

}
