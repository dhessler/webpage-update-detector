package com.solid.wud.utilities;

import java.io.File;
import java.io.Serializable;

public abstract class AssetHolder implements Serializable {
	private static final long serialVersionUID = 1L;
	private String path;

	public AssetHolder(String folderPath) {
		this.path = folderPath;
	}
	
	public Object get(String identifier) {
		File asset = this.getAsset(identifier);
		return this.onGet(asset);
	}

	public void set(String identifier, Object content) {
		File asset = this.getAsset(identifier);
		this.onSet(asset, content);
	}

	public boolean remove(String identifier) {
		File asset = this.getAsset(identifier);
		return asset.delete();
	}
	
	private File getAsset(String identifier) {
		String name = this.getAssetFileName(identifier);
		return new File(this.path, name);
	}

	protected abstract Object onGet(File asset);

	protected abstract void onSet(File asset, Object content);

	protected abstract String getAssetFileName(String fromIdentifier);
}
