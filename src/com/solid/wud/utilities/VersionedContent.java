package com.solid.wud.utilities;

public interface VersionedContent extends VersionPossessor {
	public boolean isEmpty();
	public void load();
	public void save();
}
