package com.solid.wud.utilities;

import android.text.Spannable;
import android.text.style.ForegroundColorSpan;
import android.view.ViewTreeObserver;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.widget.TextView;

public class TextViewTools {
	public static void appendColoredText(TextView textView, String text, int color) {
		int start = textView.getText().length();
		textView.append(text);
		int end = textView.getText().length();

		Spannable spannableText = (Spannable) textView.getText();
		spannableText.setSpan(new ForegroundColorSpan(color), start, end, 0);
	}

	public static void setTextWithLineLimit(final TextView textView, String text, final int limit) {
		textView.setText(text);
		ViewTreeObserver vto = textView.getViewTreeObserver();
		vto.addOnGlobalLayoutListener(new OnGlobalLayoutListener() {
			@SuppressWarnings("deprecation")
			@Override
			public void onGlobalLayout() {
				ViewTreeObserver obs = textView.getViewTreeObserver();
				obs.removeGlobalOnLayoutListener(this);
				if (textView.getLineCount() > limit) {
					int lineEndIndex = textView.getLayout().getLineEnd(limit - 1);
					String text = textView.getText().subSequence(0, lineEndIndex - 3) + "...";
					textView.setText(text);
				}

			}
		});
	}
}
