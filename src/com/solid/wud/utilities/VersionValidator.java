package com.solid.wud.utilities;


public abstract class VersionValidator<T extends VersionPossessor> {
	private T context;

	public VersionValidator(T context) {
		this.context = context;
	}

	public synchronized void validateToVersion(int toVersion) {
		int currentVersion = this.getContext().getVersion();
		if (toVersion <= currentVersion)
			return;
		
		while (currentVersion < toVersion) {
			this.validateToNextVersion(currentVersion);
			currentVersion++;
		}
		this.getContext().setVersion(toVersion);
		this.onValidationFinish();
	}

	protected T getContext() {
		return context;
	}

	protected abstract void validateToNextVersion(int currentVersion);
	protected abstract void onValidationFinish();

}
