package com.solid.wud.utilities;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.UnknownHostException;

import org.apache.commons.validator.routines.UrlValidator;

public class URLTools {
	private static UrlValidator urlValidator = new UrlValidator();

	/**
	 * Determines whether the url name syntax is valid.
	 * 
	 * @param urlName
	 *            name of the url to test
	 */
	public static boolean isValid(String urlName) {
		return urlValidator.isValid(urlName);
	}

	/**
	 * Determines whether the url is reachable, a default timeout of 10 seconds
	 * is set.
	 * 
	 * @param urlName
	 *            name of the url to test
	 * @throws UnknownHostException
	 *             if no internet connection is present
	 */
	public static boolean isReachable(String urlName) throws UnknownHostException {
		try {
			URL url = new URL(urlName);
			InputStream is = url.openStream();
			is.close();
			return true;
		} catch (IOException e) {
			if (!isInternetReachable())
				throw new UnknownHostException();
			return false;
		}
	}

	public static String getHost(String url) {
		if (url == null || url.length() == 0)
			return "";

		int doubleslash = url.indexOf("//");
		if (doubleslash == -1)
			doubleslash = 0;
		else
			doubleslash += 2;

		int end = url.indexOf('/', doubleslash);
		end = end >= 0 ? end : url.length();

		return url.substring(doubleslash, end);
	}

	private static boolean isInternetReachable() {
		try {
			URL url = new URL("http://www.google.com");
			HttpURLConnection urlConnect = (HttpURLConnection) url.openConnection();
			urlConnect.setConnectTimeout(15000);
			urlConnect.getContent();
			urlConnect.disconnect();
		} catch (UnknownHostException e) {
			return false;
		} catch (IOException e) {
			return false;
		}
		return true;
	}

}
