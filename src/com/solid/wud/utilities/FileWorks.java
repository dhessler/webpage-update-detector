package com.solid.wud.utilities;

import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.nio.channels.FileChannel;
/**
 * FileWorks offers useful methods to manage files (reading & copying).
 * @author David Hessler
 */
public final class FileWorks {
	/**
	 * Copies the content of sourceFile to destFile.
	 * @param sourceFile the source file.
	 * @param destFile the destined file to be created/overwritten.
	 */
	public static final void copy(File sourceFile, File destFile) {
		try {
			destFile.createNewFile();
			FileChannel source = null;
			FileChannel destination = null;
			try {
				source = new FileInputStream(sourceFile).getChannel();
				destination = new FileOutputStream(destFile).getChannel();
				destination.transferFrom(source, 0, source.size());
			} finally {
				if (source != null)
					source.close();
				if (destination != null)
					destination.close();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Reads a file and returns it's content.
	 * @param file the file to be read.
	 * @return a string containing the content.
	 */
	public static final String read(File file) {
		ByteArrayOutputStream os = new ByteArrayOutputStream();
		byte buf[] = new byte[1024];
		int len;
		try {
			InputStream is = new FileInputStream(file);
			while ((len = is.read(buf)) != -1) {
				os.write(buf, 0, len);
			}
			os.close();
			is.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return os.toString();
	}

	public static void write(File file, String text) {
		Writer writer = null;
		try {
		    writer = new BufferedWriter(new OutputStreamWriter(
		          new FileOutputStream(file), "UTF-8"));
		    writer.write(text);
		} catch (IOException e){
		  e.printStackTrace();
		} finally {
		   try {
			   writer.close();
		   } catch (Exception e) {
			   e.printStackTrace();
		   }
		}
		
	}
}
