package com.solid.wud.utilities;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Bundle;

public class MetaData {
	public static String getValue(Context context, String name) throws NameNotFoundException {
		ApplicationInfo ai = context.getPackageManager().getApplicationInfo(context.getPackageName(), PackageManager.GET_META_DATA);
		Bundle bundle = ai.metaData;
		return bundle.get(name).toString();
	}
}
