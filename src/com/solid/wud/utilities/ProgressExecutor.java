package com.solid.wud.utilities;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;

public abstract class ProgressExecutor extends AsyncTask<Void, Void, Void> {
	private ProgressDialog progressDialog;
	private Context context;
	private String title;
	private String message;

	public ProgressExecutor(Context context, String title, String message) {
		this.context = context;
		this.title = title;
		this.message = message;
	}

	@Override
	protected void onPreExecute() {
		super.onPreExecute();
		progressDialog = ProgressDialog.show(context, title, message, true);
	}
	
	@Override
	protected Void doInBackground(Void... args) {
		this.onExecution();
		return null;
	}

	@Override
	protected void onPostExecute(Void arg) {
		super.onPostExecute(arg);
		progressDialog.dismiss();
	}
	
	public void execute() {
		this.execute(null, null, null);
	}
	
	public abstract void onExecution();
}
